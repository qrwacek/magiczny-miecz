import { RootState } from "../store";

export const selectGameState = (state: RootState) => {
    return state.game;
}

export const selectDiceRoll = (state: RootState) => {
    return selectGameState(state).diceRoll;
}

export const selectDiceRollResult = (state: RootState) => {
    return selectGameState(state).diceRollResult;
}

export const selectInfo = (state: RootState) => {
    return selectGameState(state).info;
}

export const selectInfoTitle = (state: RootState) => {
    return selectGameState(state).infoTitle;
}

export const selectInfoCta = (state: RootState) => {
    return selectGameState(state).infoCta;
}

export const selectOptions = (state: RootState) => {
    return selectGameState(state).options;
}

export const selectOptionsTitle = (state: RootState) => {
    return selectGameState(state).optionsTitle;
}

export const selectOptionsCta = (state: RootState) => {
    return selectGameState(state).optionsCta;
}

export const selectSelectedOption = (state: RootState) => {
    return selectGameState(state).selectedOption;
}
