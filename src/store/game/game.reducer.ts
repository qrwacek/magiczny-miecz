import { Reducer } from 'redux';
import {
    IGameState,
    DICE_ROLL,
    SET_DICE_ROLL_RESULT,
    SET_INFO,
    SET_OPTIONS,
    SET_SELECTED_OPTION,
} from './game.const';
import { GameAction } from './game.actions';

const initialState: IGameState = {
    diceRoll: 0,
    diceRollResult: 0,
    info: '',
    infoTitle: '',
    infoCta: '',
    options: null,
    optionsTitle: '',
    optionsCta: '',
    selectedOption: null,
};

export const gameReducer: Reducer<IGameState, GameAction> = (
    state = initialState,
    action,
) => {
    switch (action.type) {
        case DICE_ROLL:
            return {
                ...state,
                diceRoll: action.payload,
                diceRollResult: 0,
            };
        case SET_DICE_ROLL_RESULT:
            return {
                ...state,
                diceRoll: 0,
                diceRollResult: action.payload,
            };
        case SET_INFO:
            return {
                ...state,
                info: action.payload.info,
                infoTitle: action.payload.infoTitle,
                infoCta: action.payload.infoCta,
            };
        case SET_OPTIONS:
            return {
                ...state,
                selectedOption: null,
                options: action.payload.options,
                optionsTitle: action.payload.optionsTitle,
                optionsCta: action.payload.optionsCta,
            };
        case SET_SELECTED_OPTION:
            return {
                ...state,
                options: null,
                optionsTitle: '',
                optionsCta: '',
                selectedOption: action.payload,
            };
        default:
            return state;
    }
};
