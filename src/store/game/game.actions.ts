import {
    DICE_ROLL,
    SET_DICE_ROLL_RESULT,
    SET_INFO,
    SET_OPTIONS,
    SET_SELECTED_OPTION,
    IGameState,
    IModalOptions,
} from './game.const';

export interface DiceRollAction {
    type: typeof DICE_ROLL;
    payload: IGameState['diceRoll'];
}

export interface SetDiceRollResultAction {
    type: typeof SET_DICE_ROLL_RESULT;
    payload: IGameState['diceRollResult'];
}

export interface SetInfoAction {
    type: typeof SET_INFO;
    payload: Pick<IGameState, 'info' | 'infoTitle' | 'infoCta'>;
}

export interface SetOptionsAction {
    type: typeof SET_OPTIONS;
    payload: Pick<IGameState, 'options' | 'optionsTitle' | 'optionsCta'>;
}

export interface SetSelectedOptionAction {
    type: typeof SET_SELECTED_OPTION;
    payload: IGameState['selectedOption'];
}

export type GameAction =
    | DiceRollAction
    | SetDiceRollResultAction
    | SetInfoAction
    | SetSelectedOptionAction
    | SetOptionsAction;

export function diceRoll(nrOfDice: number): DiceRollAction {
    return {
        type: DICE_ROLL,
        payload: nrOfDice,
    };
}

export function setDiceRollResult(result: number): SetDiceRollResultAction {
    return {
        type: SET_DICE_ROLL_RESULT,
        payload: result,
    };
}

export function setInfo(
    info: string,
    modalOptions: IModalOptions = {},
): SetInfoAction {
    return {
        type: SET_INFO,
        payload: {
            info,
            infoTitle: modalOptions.title || '',
            infoCta: modalOptions.cta || '',
        },
    };
}

export function setOptions(
    options: string[],
    modalOptions: IModalOptions = {},
): SetOptionsAction {
    return {
        type: SET_OPTIONS,
        payload: {
            options,
            optionsTitle: modalOptions.title || '',
            optionsCta: modalOptions.cta || '',
        },
    };
}

export function setSelectedOption(option: number): SetSelectedOptionAction {
    return {
        type: SET_SELECTED_OPTION,
        payload: option,
    };
}
