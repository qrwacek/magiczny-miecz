export const DICE_ROLL = 'DICE_ROLL';
export const SET_DICE_ROLL_RESULT = 'SET_DICE_ROLL_RESULT';
export const SET_INFO = 'SET_INFO';
export const SET_OPTIONS = 'SET_OPTIONS';
export const SET_SELECTED_OPTION = 'SET_SELECTED_OPTION';

export interface IModalOptions {
    title?: string;
    cta?: string;
}

export interface IGameState {
    diceRoll: number;
    diceRollResult: number;
    info: string;
    infoTitle: string;
    infoCta: string;
    options: string[] | null;
    optionsTitle: string;
    optionsCta: string;
    selectedOption: number | null;
}
