import { Reducer } from 'redux';
import { NotificationsAction } from './notifications.actions';
import {
    INotificationsState,
    ADD_NOTIFICATION,
    REMOVE_NOTIFICATION,
} from './notifications.const';

const initialState: INotificationsState = {
    notifications: [],
};

export const notificationsReducer: Reducer<
    INotificationsState,
    NotificationsAction
> = (state = initialState, action) => {
    switch (action.type) {
        case ADD_NOTIFICATION:
            return {
                notifications: [...state.notifications, action.payload],
            };
        case REMOVE_NOTIFICATION:
            return {
                notifications: [...state.notifications].splice(
                    action.payload,
                    1,
                ),
            };
        default:
            return state;
    }
};
