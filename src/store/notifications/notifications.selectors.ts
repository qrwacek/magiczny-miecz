import { RootState } from '../store';

export const selectNotificationsState = (state: RootState) => {
    return state.notifications;
}

export const selectNotifications = (state: RootState) => {
    return selectNotificationsState(state).notifications;
}
