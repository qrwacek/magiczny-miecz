import { ADD_NOTIFICATION, INotification, REMOVE_NOTIFICATION } from "./notifications.const";

interface AddNotificationAction {
    type: typeof ADD_NOTIFICATION;
    payload: INotification;
}

interface RemoveNotificationAction {
    type: typeof REMOVE_NOTIFICATION;
    payload: number;
}

export type NotificationsAction =
    | AddNotificationAction
    | RemoveNotificationAction;

export function addNotification(notification: INotification): NotificationsAction {
    return {
        type: ADD_NOTIFICATION,
        payload: notification,
    };
}

export function removeNotification(index: number): NotificationsAction {
    return {
        type: REMOVE_NOTIFICATION,
        payload: index,
    };
}
