export const ADD_NOTIFICATION = 'ADD_NOTIFICATION';
export const REMOVE_NOTIFICATION = 'REMOVE_NOTIFICATION';

export enum NotificationType {
    INFO = 'info',
    WARNING = 'warning',
    ERROR = 'error',
}

export interface INotification {
    message: string;
    type: NotificationType;
}

export interface INotificationsState {
    notifications: INotification[];
}
