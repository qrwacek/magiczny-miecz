import { RootState, store } from "../store";

export function waitForState<T>(
    stateSelector: (state: RootState) => T
): Promise<T> {
    const previousValue = stateSelector(store.getState());

    return new Promise((resolve) => {
        const unsub = store.subscribe(() => {
            const currentValue = stateSelector(store.getState());

            if (currentValue !== previousValue) {
                unsub();
                resolve(currentValue);
            }
        });
    })
}
