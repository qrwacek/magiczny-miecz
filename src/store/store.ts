import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import { notificationsReducer } from "./notifications";
import { gameReducer } from "./game";

const rootReducer = combineReducers({
    game: gameReducer,
    notifications: notificationsReducer,
});

export const store = createStore(rootReducer, applyMiddleware(thunk));

export type RootState = ReturnType<typeof rootReducer>;
