import React from "react";
import { Container } from "@inlet/react-pixi";
import { Transition } from "react-spring/renderprops";

export interface IFadeProps {
    show?: boolean;
}

export const Fade: React.FC<IFadeProps> = (props) => {
    const { children, show } = props;

    return (
        <Transition
            items={show}
            from={{ opacity: 0 }}
            enter={{ opacity: 1 }}
            leave={{ opacity: 0 }}
        >
            {(show) => (!!show && ((style) => (
                <Container alpha={style.opacity}>
                    {children}
                </Container>
            )))}
        </Transition>
    );
}
