import React, { useCallback } from "react";
import { Container as PixiContainer } from "pixi.js";
import { Container, _ReactPixi } from "@inlet/react-pixi";
import { Alignment, alignElement } from "board/align";

interface IWrapperProps extends _ReactPixi.IContainer {
    gutter?: number;
    horizontal?: boolean;
    alignHorizontal?: Alignment;
    alignVertical?: Alignment;
}

const DEFAULT_GUTTER = 10;

/**
 * Container displaying children relatively
 */
export const Wrapper: React.FC<IWrapperProps> = (props) => {
    const {
        gutter = DEFAULT_GUTTER,
        horizontal = false,
        alignHorizontal,
        alignVertical,
        ...restProps
    } = props;
    
    const containerRef = useCallback((container: PixiContainer) => {
        if (!container || !props.children) {
            return;
        }

        container.children.forEach((child, index, children) => {
            if (index === 0) {
                return; //skip first element
            }
            const previousChild = children[index - 1] as PixiContainer;
            if (horizontal) {
                child.x = previousChild.x + previousChild.width + gutter;
            } else {
                child.y = previousChild.y + previousChild.height + gutter;
            }
        });

        if (alignHorizontal || alignVertical) {
            alignElement(container, {
                horizontal: alignHorizontal,
                vertical: alignVertical,
            });
        }
    }, [gutter, horizontal, alignHorizontal, alignVertical, props.children]);

    return (
        <Container {...restProps} ref={containerRef as any} />
    );
}
