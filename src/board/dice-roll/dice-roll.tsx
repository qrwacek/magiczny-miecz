import React, { useState } from "react";
import { Sprite } from "@inlet/react-pixi";
import { getImageFromSprite } from "utils/assets"
import { GameAsset } from "config/assets";
import { Wrapper } from "../wrapper";
import { Button, ButtonType } from "../button";
import { getRandomNumber } from "utils/numbers";

const CHANGE_INTERVAL_MS = 100;
const ROLL_DURATION_MS = 1000;

function getDiceTexture(number: number) {
    return getImageFromSprite(GameAsset.DICE, `dice${number}`);
}

interface IDiceRollProps {
    numberOfDice: number;
    onRoll: (result: number) => void;
}

export const DiceRoll: React.FC<IDiceRollProps> = (props) => {
    const { numberOfDice, onRoll } = props;

    const numbersPlaceholder = Array(numberOfDice).fill(null);

    const [numbers, setNumbers] = useState(numbersPlaceholder.map(() => 0));
    const [done, setDone] = useState(false);
    const [isRolling, setIsRolling] = useState(false);

    const roll = () => {
        setIsRolling(true);
        const timer = setInterval(() => {
            setNumbers(numbers.map(() => getRandomNumber(1, numberOfDice * 6)));
        }, CHANGE_INTERVAL_MS);
        window.setTimeout(() => {
            clearInterval(timer);
            setIsRolling(false);
            setDone(true);
        }, ROLL_DURATION_MS)
    }

    const rollHandler = () => {
        const result = numbers.reduce((sum, n) => sum + n, 0);
        onRoll(result);
    }

    return (
        <Wrapper
            horizontal
            pointerup={() => {}}
        >
            {numbers.map((number, index) => (
                <Sprite key={index} texture={getDiceTexture(number || 1)} />
            ))}
            {done ? (
                <Button
                    type={ButtonType.CONFIRM}
                    onClick={rollHandler}
                    small
                />
            ) : (
                <Button
                    type={ButtonType.RIGHT}
                    onClick={roll}
                    disabled={isRolling}
                    small
                />
            )}
        </Wrapper>
    );
}
