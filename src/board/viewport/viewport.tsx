import { Viewport as PixiViewport } from 'pixi-viewport';
import { PixiComponent } from '@inlet/react-pixi';

interface IViewportProps {
    screenWidth: number;
    screenHeight: number;
    worldWidth: number;
    worldHeight: number;
    zoom?: number;
    centerX?: number;
    centerY?: number;
}

export const Viewport = PixiComponent<IViewportProps, PixiViewport>('Viewport', {
    create(props) {
        const {
            screenWidth,
            screenHeight,
            worldWidth,
            worldHeight,
            zoom,
            centerX = 0,
            centerY = 0,
        } = props;

        const viewport = new PixiViewport({
            screenWidth,
            screenHeight,
            worldWidth,
            worldHeight,
        });

        viewport
            .drag()
            // .pinch()
            // .wheel()
            .decelerate()
            .clamp({
                direction: 'all',
            })
            .clampZoom({
                minWidth: screenWidth,
                minHeight: screenHeight,
                maxWidth: worldWidth,
                maxHeight: worldHeight,
            })
            .fitWidth()
        ;

        if (zoom) {
            viewport.setZoom(zoom)
        }

        if (centerX || centerY) {
            viewport.moveCenter(viewport.toWorld(centerX, centerY));
        }

        return viewport;
    },

    applyProps(viewport, oldProps, props) {
        const {
            screenWidth,
            screenHeight,
            zoom,
            centerX = 0,
            centerY = 0,
        } = props;

        if (
            oldProps.screenWidth !== screenWidth ||
            oldProps.screenHeight !== screenHeight
        ) {
            viewport.resize(screenWidth, screenHeight);
        }

        if (zoom && oldProps.zoom !== zoom) {
            viewport.setZoom(zoom);
        }

        if (centerX && centerY && (
            oldProps.centerX !== centerX || oldProps.centerY !== centerY
        )) {
            viewport.moveCenter(viewport.toWorld(centerX, centerY))
        }
    },
});
