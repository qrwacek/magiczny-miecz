import React, { useState } from "react";
import { Sprite, Container } from "@inlet/react-pixi";
import { GameAsset } from 'config/assets';
import { GUTTER } from "config/style";
import { getImage } from "utils/assets";
import { useWindowSize } from "utils/hooks";

import { Viewport } from "../viewport";
import { Button, ButtonType } from "../button";
import { Wrapper } from "../wrapper";

const MAX_ZOOM = 1;
const MIN_ZOOM = 0.4;

export const World: React.FC = () => {
    const [zoom, setZoom] = useState(1);
    const { width, height } = useWindowSize();

    const boardSprite = getImage(GameAsset.BOARD);

    const worldWidth = boardSprite.width;
    const worldHeight = boardSprite.height;

    return (
        <Container>
            <Viewport
                screenWidth={width}
                screenHeight={height}
                worldWidth={worldWidth}
                worldHeight={worldHeight}
                zoom={zoom}
            >
                <Sprite texture={boardSprite} />
            </Viewport>
            <Wrapper x={GUTTER} y={GUTTER}>
                <Button
                    type={ButtonType.ADD}
                    onClick={() => setZoom(zoom + 0.1)}
                    tooltip={'Zoom in'}
                    disabled={zoom >= MAX_ZOOM}
                    small
                />
                <Button
                    type={ButtonType.REMOVE}
                    onClick={() => setZoom(zoom - 0.1)}
                    tooltip={'Zoom out'}
                    disabled={zoom <= MIN_ZOOM}
                    small
                />
            </Wrapper>
        </Container>
    );
};
