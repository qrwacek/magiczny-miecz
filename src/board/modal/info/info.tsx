import React, { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useSelector, useDispatch } from "react-redux";

import { selectInfo, selectInfoTitle, selectInfoCta, setInfo } from "store/game";

import { Modal } from "../modal";
import { TextButton } from "../../button";

export const InfoModal: React.FC = () => {
    const { t } = useTranslation();

    const info = useSelector(selectInfo);
    const infoTitle = useSelector(selectInfoTitle);
    const infoCta = useSelector(selectInfoCta);

    const dispatch = useDispatch();

    const infoConfirmHandler = useCallback(() => {
        dispatch(setInfo(''));
    }, [dispatch]);

    if (!info) {
        return null;
    }

    return (
        <Modal
            title={infoTitle || t('info.title')}
            content={info}
            footer={() => (
                <TextButton
                    text={infoCta || t('info.confirm')}
                    onClick={infoConfirmHandler}
                />
            )}
            vertical
        />
    );
}
