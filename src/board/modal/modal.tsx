import React from "react";
import { Container, Sprite, Text } from "@inlet/react-pixi"
import { TextStyle, TextMetrics } from "pixi.js";

import { BLACK } from "const";
import { getImageFromSprite } from "utils/assets";
import { GameAsset, FONT_HEADER } from "config";
import { useWindowSize } from "utils/hooks/use-window-size";
import { Background } from "../background";
import { Align, Alignment } from "../align";
// import { Scrollbox } from "../scrollbox";

const HEADER_OFFSET = 50;

const CONTENT_MARGIN = 100;
const CONTENT_MARGIN_VERTICAL = 90;
const CONTENT_OFFSET = 120;
const CONTENT_OFFSET_VERTICAL = 105;
const CONTENT_PADDING = 20;
const CONTENT_BG = 0xCFAE60;

interface IModalHeaderProps {
    title: string;
    width: number;
    vertical?: boolean;
}

const ModalHeader: React.FC<IModalHeaderProps> = (props) => {
    const { title, width, vertical } = props;

    if (!title) return null;

    const header = getImageFromSprite(
        GameAsset.WINDOWS,
        vertical ? 'modal-vertical-header' : 'modal-horizontal-header',
    );

    const textStyle = new TextStyle({
        fill: [0xa4beb9, 0xbccecb],
        fontFamily: FONT_HEADER,
        fontSize: 34,
        stroke: 0x899c99,
        strokeThickness: 3,

        wordWrap: true,
        wordWrapWidth: 280,
        align: 'center',
    });

    // get metrics to center text vertically and horizontally
    const textMetrics = TextMetrics.measureText(title, textStyle);
    
    return (
        <Container
            x={vertical ? 0 : (header.width - width) / -2}
        >
            <Sprite texture={header} />
            <Text
                text={title}
                style={textStyle}
                x={(header.width - textMetrics.width) / 2}
                y={(110 - textMetrics.height) / 2}
            />
        </Container>
    )
}

interface IModalFooterProps {
    width: number;
    height: number;
    vertical?: boolean;
}

const ModalFooter: React.FC<IModalFooterProps> = (props) => {
    const { width, height, vertical, children } = props;

    const footer = getImageFromSprite(
        GameAsset.WINDOWS,
        vertical ? 'modal-vertical-footer' : 'modal-horizontal-footer',
    );

    return (
        <Container
            x={(width - footer.width) / 2}
            y={height - (vertical ? 85 : 105)}
        >
            <Sprite texture={footer} />
            <Align
                horizontal={Alignment.CENTER}
                vertical={Alignment.CENTER}
            >
                {children}
            </Align>
        </Container>
    );
}

interface IModalProps {
    title?: string;
    content?: string;
    contentStyle?: TextStyle;
    footer?: React.FC,
    vertical?: boolean;
    noBackground?: boolean;
    contentAlignHorizontal?: Alignment;
    contentAlignVertical?: Alignment;
}

export const Modal: React.FC<IModalProps> = (props) => {
    const {
        vertical,
        title,
        content,
        contentStyle,
        noBackground,
        children,
        contentAlignHorizontal,
        contentAlignVertical,
        footer: Footer,
    } = props;

    const { width, height } = useWindowSize();

    const body = getImageFromSprite(
        GameAsset.WINDOWS,
        vertical ? 'modal-vertical-body' : 'modal-horizontal-body',
    );
    const header = getImageFromSprite(
        GameAsset.WINDOWS,
        vertical ? 'modal-vertical-header' : 'modal-horizontal-header',
    );

    const scaleX = width / (body.width + CONTENT_PADDING * 2);
    const scaleY = height / (body.height + CONTENT_PADDING * 2);
    const scale = Math.min(1, scaleX, scaleY);

    const headerOffsetTop = title ? HEADER_OFFSET : 0;
    const headerOffsetLeft = title && vertical ? (header.width - body.width) / 2 : 0;

    const contentOffset = vertical ? CONTENT_OFFSET_VERTICAL : CONTENT_OFFSET;
    const contentMargin = vertical ? CONTENT_MARGIN_VERTICAL : CONTENT_MARGIN;

    const contentWidth = body.width - 2 * contentMargin;
    const contentHeight = body.height - 2 * contentOffset;

    return (
        <Container interactive>
            <Background
                color={BLACK}
                alpha={0.5}
                width={width}
                height={height}
            />
            <Align
                horizontal={Alignment.CENTER}
                vertical={Alignment.CENTER}
            >
                <Container scale={scale}>
                    <Sprite
                        texture={body}
                        x={headerOffsetLeft}
                        y={headerOffsetTop}
                    />
                    {!!title && (
                        <ModalHeader
                            title={title}
                            width={body.width}
                            vertical={vertical}
                        />
                    )}
                    <Container
                        x={contentMargin + headerOffsetLeft}
                        y={contentOffset + headerOffsetTop}
                    >
                        <Background
                            color={CONTENT_BG}
                            alpha={noBackground ? 0 : 1}
                            width={contentWidth}
                            height={contentHeight}
                            rounded={10}
                        />
                        {!!content ? (
                            // <Scrollbox
                            //     width={contentWidth - 2 * CONTENT_PADDING}
                            //     height={contentHeight - 2 * CONTENT_PADDING}
                            // >
                                <Text
                                    text={content}
                                    style={new TextStyle({
                                        wordWrap: true,
                                        wordWrapWidth: contentWidth - 2 * CONTENT_PADDING,
                                        ...contentStyle,
                                    })}
                                    x={CONTENT_PADDING}
                                    y={CONTENT_PADDING}
                                />
                            // </Scrollbox>
                        ) : (
                            <Align
                                horizontal={contentAlignHorizontal}
                                vertical={contentAlignVertical}
                            >
                                {children}
                            </Align>
                        )}
                    </Container>
                    {!!Footer && (
                        <ModalFooter
                            width={body.width + 2 * headerOffsetLeft}
                            height={body.height + headerOffsetTop}
                            vertical={vertical}
                        >
                            <Footer />
                        </ModalFooter>
                    )}
                </Container>
            </Align>
        </Container>
    );
}
