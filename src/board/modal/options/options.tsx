import React, { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useSelector, useDispatch } from "react-redux";
import { Text } from "@inlet/react-pixi";

import { selectOptions, selectOptionsTitle, setSelectedOption } from "store/game";

import { Modal } from "../modal";
import { Wrapper } from "../../wrapper";

export const OptionsModal: React.FC = () => {
    const { t } = useTranslation();

    const options = useSelector(selectOptions);
    const optionsTitle = useSelector(selectOptionsTitle);

    const dispatch = useDispatch();

    const selectedOptionHandler = useCallback((option: number) => {
        dispatch(setSelectedOption(option));
    }, [dispatch]);

    if (!options) {
        return null;
    }

    return (
        <Modal
            title={optionsTitle || t('options.title')}
            vertical
        >
            <Wrapper>
                {options.map((option, index) => (
                    <Text
                        text={option}
                        pointerup={() => selectedOptionHandler(index)}
                    />
                ))}
            </Wrapper>
        </Modal>
    );
}
