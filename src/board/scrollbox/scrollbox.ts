import { PixiComponent } from "@inlet/react-pixi";
import { Scrollbox as PixiScrollbox } from 'pixi-scrollbox'
import { Viewport } from "pixi-viewport";

interface IScrollboxProps {
    width: number;
    height: number;
}

export const Scrollbox = PixiComponent<IScrollboxProps, Viewport>('Scrollbox', {
    create(props) {
        const { width, height } = props;

        const scrollbox = new PixiScrollbox({
            boxWidth: width,
            boxHeight: height,
            scrollbarForeground: 0x00ff00,
            scrollbarForegroundAlpha: 1,
        });

        return scrollbox.content;
    },

    applyProps(scrollboxContent, oldProps, props) {
        const { width, height } = props;

        const scrollbox = scrollboxContent.parent as PixiScrollbox;

        if (width !== oldProps.width || height !== oldProps.height) {
            scrollbox.resize({
                boxWidth: width,
                boxHeight: height,
            });
        }

        scrollbox.update();
    },
});
