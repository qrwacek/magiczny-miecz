import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { rollTheDice, chooseOption } from 'game/utils';
import { GameStage } from 'game';
import { useGame } from 'utils/hooks';

import { Wrapper } from '../wrapper';
import { Button, ButtonType } from '../button';
import { TooltipAlign } from '../tooltip';

export const Actions: React.FC = () => {
    const { G, ctx, moves, currentPlayer, currentStage } = useGame();
    const { t } = useTranslation();

    const pvpAvailable = Object.values(G.players!).some((player) => {
        return (
            player.name !== currentPlayer.name &&
            player.location === currentPlayer.location
        );
    });

    const move = useCallback(async () => {
        await rollTheDice(1);
        const newLocation = currentPlayer.location!;
        moves.moveTo(newLocation);
    }, [currentPlayer, moves]);

    const explore = useCallback(async () => {
        const option = await chooseOption(['Option 1', 'Option 2', 'Option 3']);
        console.log(option);
        ctx.events.endStage();
    }, [ctx]);

    const fight = useCallback(() => {
        console.log('fight');
        ctx.events.endStage();
    }, [ctx]);

    const end = useCallback(() => {
        moves.end();
    }, [moves]);

    return (
        <Wrapper>
            {currentStage === GameStage.START && (
                <Button
                    type={ButtonType.RIGHT}
                    tooltip={t('actions.move')}
                    tooltipAlign={TooltipAlign.LEFT}
                    onClick={move}
                    small
                />
            )}
            {currentStage === GameStage.MEET_OR_EXPLORE && (
                <Wrapper>
                    <Button
                        type={ButtonType.RIGHT}
                        tooltip={t('actions.explore')}
                        tooltipAlign={TooltipAlign.LEFT}
                        onClick={explore}
                        small
                    />
                    {pvpAvailable && (
                        <Button
                            type={ButtonType.PLAYER}
                            tooltip={t('actions.fight')}
                            tooltipAlign={TooltipAlign.LEFT}
                            onClick={fight}
                            small
                        />
                    )}
                </Wrapper>
            )}
            {currentStage === GameStage.END && (
                <Button
                    type={ButtonType.EXIT}
                    tooltip={t('actions.endTurn')}
                    tooltipAlign={TooltipAlign.LEFT}
                    onClick={end}
                    small
                />
            )}
        </Wrapper>
    );
};
