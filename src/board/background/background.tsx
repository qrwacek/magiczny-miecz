import React from "react";
import { Graphics } from "@inlet/react-pixi";

interface IBackgroundProps {
    width: number;
    height: number;
    color: number;
    alpha?: number;
    rounded?: number;
}

export const Background: React.FC<IBackgroundProps> = (props) => {
    const { color, alpha, width, height, rounded } = props;

    return (
        <Graphics
            draw={(graphics) => {
                graphics.clear();
                graphics.beginFill(color, alpha);
                if (rounded) {
                    graphics.drawRoundedRect(0, 0, width, height, rounded);
                } else {
                    graphics.drawRect(0, 0, width, height)
                }
                graphics.endFill();
            }}
        />
    );
}
