import React from "react";
import { useTranslation } from "react-i18next";
import { TextStyleOptions, TextStyle, TextMetrics } from "pixi.js";
import { Container, Text, Sprite } from "@inlet/react-pixi";

import { WHITE, BLACK } from "const";
import { CharacterId, CHARACTER_CONFIG } from "game/character";
import { LOCATIONS_CONFIG } from "game/location";
import { getCharacterImage } from "../character.utils";
import { Background } from "../../background";

// const WIDTH = 264;
// const HEIGHT = 324;
const WIDTH = 344;
const HEIGHT = 422;
const FRAME_WIDTH = 34;
const HEADING_GUTTER = 15;
const TEXT_GUTTER = 5;

const INFO_WIDTH = WIDTH - 2 * FRAME_WIDTH;
const INFO_HEIGHT = HEIGHT - 2 * FRAME_WIDTH;

const BG_COLOR = 0x106c8c;
const COLOR_TITLE = BLACK;
const COLOR_MIGHT = 0xe20000;
const COLOR_MAGIC = 0x3c1d6a;
const COLOR_GOLD = 0xfffd3b;
const COLOR_LIFE = 0x1f5e1c;

interface ICharacterCardProps {
    character: CharacterId;
}

const headingStyle: TextStyleOptions = {
    fontFamily: '"Times New Roman", Times, serif',
    fontSize: 23,
    stroke: WHITE,
    strokeThickness: 3,
    lineHeight: FRAME_WIDTH
};

function createHeadingStyle(color?: number) {
    return new TextStyle({
        ...headingStyle,
        fill: color
    });
}

const textStyle: TextStyleOptions = {
    fontFamily: '"Times New Roman", Times, serif',
    fontSize: 14
};

export const CharacterCard: React.FC<ICharacterCardProps> = props => {
    const { character } = props;

    const { t } = useTranslation();

    const characterImage = getCharacterImage(character);
    const characterConfig = CHARACTER_CONFIG[character];
    const { description = '' } = characterConfig

    const location = LOCATIONS_CONFIG[characterConfig.location]!;

    const info =
        t("characterCard.info.nature.heading", {
            nature: t(
                `characterCard.info.nature.type.${characterConfig.nature}`
            )
        }) +
        "\n" +
        t("characterCard.info.location", {
            location: location.name
        })
    ;

    const descMaxHeight = INFO_HEIGHT - characterImage.height - 3 * TEXT_GUTTER;
    const descriptionStyle = new TextStyle({
        ...textStyle,
        wordWrap: true,
        wordWrapWidth: INFO_WIDTH - 2 * TEXT_GUTTER,
    });
    const descriptionMetrics = TextMetrics.measureText(
        description || "",
        descriptionStyle,
    );
    const { lineHeight, lines } = descriptionMetrics;

    let DescriptionTop: any = () => null;
    let DescriptionBottom: any = () => null;

    const bottomLines = Math.floor(descMaxHeight / lineHeight);

    if (lines.length <= bottomLines) {
        DescriptionBottom = () => (
            <Text
                text={description}
                style={descriptionStyle}
                x={TEXT_GUTTER}
                y={characterImage.height + 2 * TEXT_GUTTER}
            />
        );
    } else {
        const topText = description.slice(0, description.indexOf(
            lines[lines.length - bottomLines]
        )).trim();
        const bottomText = description.slice(description.indexOf(
            lines[lines.length - bottomLines]
        )).trim();

        DescriptionTop = () => (
            <Text
                text={topText}
                style={new TextStyle({
                    ...textStyle,
                    wordWrap: true,
                    wordWrapWidth: INFO_WIDTH - characterImage.width - 3 * TEXT_GUTTER,
                })}
                x={characterImage.width + 2 * TEXT_GUTTER}
                y={characterImage.height + 2 * TEXT_GUTTER}
                anchor={[0, 1]}
            />
        );
        DescriptionBottom = () =>(
            <Text
                text={bottomText}
                style={descriptionStyle}
                x={TEXT_GUTTER}
                y={characterImage.height + 2 * TEXT_GUTTER}
            />
        );
    }

    return (
        <Container>
            <Background color={BG_COLOR} width={WIDTH} height={HEIGHT} />
            <Text
                text={characterConfig.name.toUpperCase()}
                style={createHeadingStyle(COLOR_TITLE)}
                x={WIDTH / 2}
                anchor={[0.5, 0]}
            />
            <Text
                text={t("characterCard.attributes.might", {
                    might: characterConfig.might
                })}
                style={createHeadingStyle(COLOR_MIGHT)}
                angle={90}
                x={FRAME_WIDTH}
                y={FRAME_WIDTH + HEADING_GUTTER}
            />
            <Text
                text={t("characterCard.attributes.magic", {
                    magic: characterConfig.magic
                })}
                style={createHeadingStyle(COLOR_MAGIC)}
                angle={90}
                x={FRAME_WIDTH}
                y={HEIGHT - FRAME_WIDTH - HEADING_GUTTER}
                anchor={[1, 0]}
            />
            <Text
                text={t("characterCard.attributes.gold")}
                style={createHeadingStyle(COLOR_GOLD)}
                angle={-90}
                x={WIDTH - FRAME_WIDTH}
                y={FRAME_WIDTH + HEADING_GUTTER}
                anchor={[1, 0]}
            />
            <Text
                text={t("characterCard.attributes.life")}
                style={createHeadingStyle(COLOR_LIFE)}
                angle={-90}
                x={WIDTH - FRAME_WIDTH}
                y={HEIGHT - FRAME_WIDTH - HEADING_GUTTER}
            />
            <Container x={FRAME_WIDTH} y={FRAME_WIDTH}>
                <Background
                    color={WHITE}
                    width={INFO_WIDTH}
                    height={INFO_HEIGHT}
                />
                <Sprite
                    texture={characterImage}
                    x={TEXT_GUTTER}
                    y={TEXT_GUTTER}
                />
                <Text
                    text={info}
                    style={
                        new TextStyle({
                            ...textStyle,
                            align: "right"
                        })
                    }
                    x={INFO_WIDTH - TEXT_GUTTER}
                    y={2 * TEXT_GUTTER}
                    anchor={[1, 0]}
                />
                <DescriptionTop />
                <DescriptionBottom />
            </Container>
        </Container>
    );
};
