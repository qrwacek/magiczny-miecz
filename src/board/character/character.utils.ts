import { CharacterId } from "game/character";
import { getAsset } from "utils/assets";
import { GameAsset } from "config";

export function getCharacterImage(characterId: CharacterId) {
    return getAsset(GameAsset.CHARACTERS).textures![`${characterId}.png`]
}
