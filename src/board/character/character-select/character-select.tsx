import React, { useState } from "react";
import { useTranslation } from "react-i18next";

import { CharacterId, ALL_CHARACTERS } from "game/character";
import { Button, ButtonType, TextButton } from "board/button";
import { TooltipAlign } from "board/tooltip";
import { Modal } from "board/modal";
import { Wrapper } from "board/wrapper";
import { CharacterCard } from "../character-card";
import { Alignment } from "board/align";
import { useWindowSize } from "utils/hooks";

interface ICharacterSelectProps {
    onSelect: (character: CharacterId) => void;
    exclude?: CharacterId[];
}

export const CharacterSelect: React.FC<ICharacterSelectProps> = (props) => {
    const { exclude = [], onSelect } = props;

    const [selected, setSelected] = useState(0);
    const { t } = useTranslation();
    const { isMobile } = useWindowSize();

    const characters = ALL_CHARACTERS.filter((item) => !exclude.includes(item));

    const selectedCharacter = characters[selected];

    function showNext() {
        setSelected((selected + 1) % characters.length)
    }

    function showPrevious() {
        setSelected(selected > 0 ? selected - 1 : characters.length - 1);
    }

    return (
        <Modal
            title={t('characterSelect.title')}
            footer={() => (
                <Wrapper horizontal>
                    <Button
                        type={ButtonType.ARROW_LEFT}
                        onClick={showPrevious}
                        tooltip={t('characterSelect.previous')}
                        tooltipAlign={TooltipAlign.TOP}
                    />
                    <TextButton
                        text={t('characterSelect.select')}
                        onClick={() => onSelect(selectedCharacter)}
                    />
                    <Button
                        type={ButtonType.ARROW_RIGHT}
                        onClick={showNext}
                        tooltip={t('characterSelect.next')}
                        tooltipAlign={TooltipAlign.TOP}
                    />
                </Wrapper>
            )}
            vertical={isMobile}
            noBackground
            contentAlignHorizontal={Alignment.CENTER}
            contentAlignVertical={Alignment.CENTER}
        >
            <CharacterCard character={selectedCharacter} />
        </Modal>
    );
}
