import { Container } from "pixi.js";
import { centerHorizontally, centerVertically, alignRight, alignBottom } from "utils/elements";
import { Alignment } from "./align.const";

export const alignElement = (container: Container, options: {
    horizontal?: Alignment,
    vertical?: Alignment,
    horizontalGutter?: number,
    verticalGutter?: number,
}) => {
    const {
        horizontal,
        vertical,
        horizontalGutter = 0,
        verticalGutter = 0,
    } = options;

    if (horizontal === Alignment.CENTER) {
        centerHorizontally(container);
    }
    if (vertical === Alignment.CENTER) {
        centerVertically(container);
    }

    if (horizontal === Alignment.END) {
        alignRight(container, horizontalGutter);
    }
    if (vertical === Alignment.END) {
        alignBottom(container, verticalGutter);
    }
}
