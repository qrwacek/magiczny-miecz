export enum Alignment {
    START = 'start',
    CENTER = 'center',
    END = 'end',
}
