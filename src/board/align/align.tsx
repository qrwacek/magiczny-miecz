import React, { useCallback } from "react"
import { Container as PixiContainer } from "pixi.js"
import { Container, _ReactPixi } from "@inlet/react-pixi"
import { alignElement } from "./align.utils";
import { Alignment } from "./align.const";

interface IAlignProps extends _ReactPixi.IContainer {
    horizontal?: Alignment;
    vertical?: Alignment;
    horizontalGutter?: number;
    verticalGutter?: number;
    debug?: boolean;
}

export const Align: React.FC<IAlignProps> = (props) => {
    const {
        horizontal,
        vertical,
        horizontalGutter = 0,
        verticalGutter = 0,
        children,
        x,
        y,
        debug,
        ...restProps
    } = props;

    const containerRef = useCallback((container: PixiContainer) => {
        if (!container || ! children) {
            return;
        }
        
        if (debug) {
            console.log('ALIGN', container.width, container.parent.width, container);
        }
        alignElement(container, {
            horizontal,
            vertical,
            horizontalGutter,
            verticalGutter,
        });
    }, [horizontal, vertical, horizontalGutter, verticalGutter, children, debug]);

    return (
        <Container {...restProps} ref={containerRef as any}>
            {children}
        </Container>
    )
}
