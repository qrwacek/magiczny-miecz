import React, { useRef, useEffect } from "react";
import * as PIXI from "pixi.js"
import { Container, Text, Graphics } from "@inlet/react-pixi"
import { WHITE, BLACK } from "const";
import { centerVertically, centerHorizontally } from "utils/elements";
import { TooltipAlign } from "./tooltip.const";

const textStyle = new PIXI.TextStyle({
    fill: WHITE,
    fontSize: 14,
    fontWeight: 'bold',
});

const DEFAULT_GUTTER = 5;
const DEFAULT_ALIGN = TooltipAlign.RIGHT;

interface ITooltipProps {
    text: string;
    align?: TooltipAlign;
    gutter?: number;
    visible?: boolean;
}

export const Tooltip: React.FC<ITooltipProps> = (props) => {
    const {
        text,
        align = DEFAULT_ALIGN,
        gutter = DEFAULT_GUTTER,
        visible,
    } = props;

    const containerRef = useRef<PIXI.Container>(null);

    const textMetrics = PIXI.TextMetrics.measureText(text, textStyle);
    const width = textMetrics.width + 2 * gutter;
    const height = textMetrics.height + 2 * gutter;

    // tooltip positioning
    useEffect(() => {
        const container = containerRef.current;

        if (!container) {
            return;
        }

        const { parent } = container;

        if (!parent) {
            return;
        }

        switch (align) {
            case TooltipAlign.TOP:
                container.y = -height - gutter;
                break;
            case TooltipAlign.BOTTOM:
                container.y = parent.height + gutter;
                break;
            case TooltipAlign.LEFT:
                container.x = -width - gutter;
                break;
            case TooltipAlign.RIGHT:
                container.x = parent.width + gutter;
                break;
            default:
                break;
        }

        if ([TooltipAlign.LEFT, TooltipAlign.RIGHT].includes(align)) {
            centerVertically(container);
        } else {
            centerHorizontally(container)
        }
    }, [containerRef, align, width, height, gutter]);

    return (
        <Container
            ref={containerRef as any}
            alpha={visible ? 1 : 0}
        >
            <Graphics
                alpha={0.5}
                draw={(graphics) => {
                    graphics
                        .beginFill(BLACK, 0.75)
                        .drawRoundedRect(0, 0, width, height, 2)
                }}
            />
            <Text text={text} style={textStyle} x={gutter} y={gutter} />
        </Container>
    );
}
