import React, { useState } from "react";
import { Container, Text } from "@inlet/react-pixi";
import { TextStyle } from "pixi.js";

import { FONT_HEADER } from "config/style";
import { Button, IButtonProps } from "../button";
import { ButtonType } from "../button.const";
import { Align, Alignment } from "board/align";

interface ITextButtonProps extends Omit<IButtonProps, "type"> {
    text: string;
}

const FONT_SIZE = 24;

const FILL = [0x6b972c, 0x85be39];
const FILL_HOVER = [0x97b738, 0xb6dd46];
const FILL_CLICK = [0x426F1D, 0x5C9A2C];
const FILL_DISABLED = [0x889596, 0xA7AFAF];

const STROKE = 0x547322;
const STROKE_HOVER = 0x7b922b;
const STROKE_CLICK = 0x2F4B14;
const STROKE_DISABLED = 0x6e7777;

export const TextButton: React.FC<ITextButtonProps> = (props) => {
    const { text, ...restProps } = props;
    const { disabled } = restProps;

    const [isHovered, setIsHovered] = useState(false);
    const [isClicked, setIsClicked] = useState(false);

    let stroke = STROKE;
    let fill = FILL;
    if (disabled) {
        stroke = STROKE_DISABLED;
        fill = FILL_DISABLED;
    } else if (isClicked) {
        stroke = STROKE_CLICK;
        fill = FILL_CLICK;
    } else if (isHovered) {
        stroke = STROKE_HOVER;
        fill = FILL_HOVER;
    }

    const textStyle = new TextStyle({
        fontFamily: FONT_HEADER,
        fontSize: FONT_SIZE,
        fill: fill,
        stroke: stroke,
        strokeThickness: 3,
    });

    return (
        <Container
            interactive
            pointerover={() => setIsHovered(true)}
            pointerout={() => setIsHovered(false)}
            pointerupoutside={() => setIsClicked(false)}
            pointerdown={() => setIsClicked(true)}
            pointerup={() => setIsClicked(false)}
        >
            <Button {...restProps} type={ButtonType.TEXT} />
            <Align
                vertical={Alignment.CENTER}
                horizontal={Alignment.CENTER}
            >
                <Text text={text} style={textStyle} />
            </Align>
        </Container>
    );
};
