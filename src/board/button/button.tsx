import React, { useState } from "react";
import { Sprite, Container, _ReactPixi } from "@inlet/react-pixi";
import { getAsset } from "utils/assets";
import { GameAsset } from "config/assets";
import { Tooltip, TooltipAlign } from "../tooltip";
import { ButtonState, ButtonType } from './button.const'

export interface IButtonProps extends _ReactPixi.IContainer {
    type: ButtonType;
    disabled?: boolean;
    tooltip?: string;
    tooltipAlign?: TooltipAlign;
    small?: boolean;
    onClick?: () => void;
}

export const Button: React.FC<IButtonProps> = (props) => {
    const {
        type,
        disabled,
        tooltip,
        tooltipAlign = TooltipAlign.RIGHT,
        small,
        onClick,
        ...restProps
    } = props;

    const [buttonState, setButtonState] = useState(ButtonState.DEFAULT);
    const [tooltipVisible, setTooltipVisible] = useState(false);

    const state = disabled ? ButtonState.DISABLED : buttonState;
    const stateKey = state === ButtonState.DEFAULT ? type : `${type}-${state}`;
    const stateImage = getAsset(GameAsset.CONTROLS).textures![`${stateKey}.png`];

    return (
        <Container
            pointerover={() => {
                setButtonState(ButtonState.HOVER);
                setTooltipVisible(true);
            }}
            pointerout={() => {
                setButtonState(ButtonState.DEFAULT);
                setTooltipVisible(false);
            }}
            pointerupoutside={() => setButtonState(ButtonState.DEFAULT)}
            pointerdown={() => setButtonState(ButtonState.CLICK)}
            pointerup={() => {
                setButtonState(ButtonState.DEFAULT);
                if (!disabled && onClick) {
                    onClick();
                }
            }}
            interactive={true}
            buttonMode={true}
            cursor={disabled ? 'default' : 'pointer'}
            {...restProps}
        >
            <Sprite
                texture={stateImage}
                scale={small ? 0.3 : 0.5}
            />
            {!!tooltip && (
                <Tooltip
                    text={tooltip}
                    align={tooltipAlign}
                    visible={tooltipVisible}
                />
            )}
        </Container>
    );
}
