import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Container as PixiContainer } from 'pixi.js';
import { Stage, Container } from '@inlet/react-pixi';
import { BoardProps } from 'boardgame.io/react';

import { GUTTER } from 'config/style';
import { useWindowSize } from 'utils/hooks';
import {
    Moves,
    IGameState,
    PlayerId,
    GamePhase,
    GameStage,
    IGameContext,
} from '../game';
import {
    selectDiceRoll,
    setDiceRollResult,
} from 'store/game';

import { World } from './world';
import { Actions } from './actions';
import { CharacterSelect } from './character/character-select';
import { DiceRoll } from './dice-roll';
import { InfoModal } from './modal/info';
import { OptionsModal } from './modal/options';
import { Fade } from './animations/fade';

import './board.css';

export const GameContext = React.createContext<IGameContext>({
    G: {} as any,
    ctx: {} as any,
    moves: {} as any,
});

export const GameBoard: React.FC<
    BoardProps<IGameState, Moves, PlayerId, GamePhase, GameStage>
> = (props) => {
    const { G, ctx, moves, isActive } = props;

    const { width, height } = useWindowSize();

    const diceRoll = useSelector(selectDiceRoll);

    const dispatch = useDispatch();

    const actionsRef = useCallback(
        (actions: PixiContainer) => {
            if (!actions) {
                return;
            }

            actions.x = width - actions.width - GUTTER;
        },
        [width],
    );

    const diceRollHandler = useCallback(
        (result: number) => {
            dispatch(setDiceRollResult(result));
        },
        [dispatch],
    );

    return (
        <Stage
            width={width}
            height={height}
            options={{
                antialias: true,
                backgroundColor: 0x000000,
            }}
        >
            <GameContext.Provider value={{ G, ctx, moves }}>
                <World />
                {isActive ? (
                    ctx.phase === GamePhase.CHARACTER_SELECT ? (
                        <CharacterSelect
                            exclude={[]}
                            onSelect={moves.chooseCharacter}
                        />
                    ) : (
                        <Container ref={actionsRef as any} y={GUTTER}>
                            <Actions />
                        </Container>
                    )
                ) : (
                    <Container />
                )}
                <Fade show={!!diceRoll}>
                    <DiceRoll
                        numberOfDice={diceRoll}
                        onRoll={diceRollHandler}
                    />
                </Fade>
                {/* <InfoModal /> */}
                {/* <OptionsModal /> */}
            </GameContext.Provider>
        </Stage>
    );
};
