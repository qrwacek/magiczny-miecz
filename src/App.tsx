import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Provider } from "react-redux";

import { loadAssets, loadFont } from "utils/assets";
import { GameAsset } from "config/assets";
import { FONT_HEADER } from "config/style";
import { store } from "store";

import { GameClient } from "./client";
import "./app.css";

const App: React.FC = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [playerId, setPlayerId] = useState("0");
    const { t } = useTranslation();

    useEffect(() => {
        loadAssets([
            GameAsset.BOARD,
            GameAsset.CONTROLS,
            GameAsset.WINDOWS,
            GameAsset.DICE,
            GameAsset.CHARACTERS,
        ]).then(() => {
            return loadFont(FONT_HEADER);
        }).then(() => {
            setIsLoading(false);
        });
    }, []);

    if (isLoading) {
        return (
            <div className="loading">
                {t('loading')}
            </div>
        );
    }

    return (
        <div className="app" id="app">
            <Provider store={store}>
                <GameClient playerID={playerId} key={playerId} />
            </Provider>
            <div className="buttons">
                <button
                    className={playerId === '0' ? 'active' : ''}
                    onClick={() => setPlayerId('0')}
                >
                    Player 0
                </button>
                <button
                    className={playerId === '1' ? 'active' : ''}
                    onClick={() => setPlayerId('1')}
                >
                    Player 1
                </button>
            </div>
        </div>
    );
};

export default App;
