export enum GameSprite {
    CONTROLS = 'controls',
}

/**
 * Sprites file names
 */
export const SPRITES: Record<GameSprite, string> = {
    [GameSprite.CONTROLS]: 'controls.json',
};
