import { BLACK } from '../const';

export const IS_FULLSCREEN: boolean = true;

/**
 * Maximum game canvas width
 *
 * @type {number}
 */
export const MAX_GAME_WIDTH = 800;

/**
 * Maximum game canvas height
 *
 * @type {number}
 */
export const MAX_GAME_HEIGHT = 600;

/**
 * Game canvas background color
 *
 * @type {number}
 */
export const GAME_BACKGROUND = BLACK;
