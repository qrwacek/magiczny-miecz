/**
 * Directory containing game assets
 *
 * @type {string}
 */
export const ASSETS_ROOT = './assets';

export enum GameAsset {
    BACKGROUND = 'background',
    BOARD = 'board',
    SCREEN_BACKGROUND = 'screen-background',
    CONTROLS = 'controls',
    BUTTON = 'button',
    DICE = 'dice',
    WINDOWS = 'windows',
    CHARACTERS = 'characters',
    EVENTS = 'events',
    ITEMS = 'items',
    SPELLS = 'spells',
}

export const GameAssets: Record<GameAsset, string> = {
    [GameAsset.BACKGROUND]: 'bg.jpg',
    [GameAsset.BOARD]: 'board.jpg',
    [GameAsset.SCREEN_BACKGROUND]: 'screen.png',
    [GameAsset.CONTROLS]: 'controls.json',
    [GameAsset.BUTTON]: 'button.json',
    [GameAsset.DICE]: 'dice.json',
    [GameAsset.WINDOWS]: 'windows.json',
    [GameAsset.CHARACTERS]: 'characters.json',
    [GameAsset.EVENTS]: 'events.json',
    [GameAsset.ITEMS]: 'items.json',
    [GameAsset.SPELLS]: 'spells.json',
};
