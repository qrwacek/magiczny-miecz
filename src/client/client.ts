import { Client } from "boardgame.io/react";
import { Game } from "../game";
import { GameBoard } from "../board";

export const GameClient = Client({
    game: Game,
    board: GameBoard,
    multiplayer: { local: true },
    // multiplayer: { server: 'localhost:8000' },
    // debug: true,
});
