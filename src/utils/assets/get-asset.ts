import { Loader } from 'pixi.js';
import { GameAsset } from '../../config/assets';

export const getAsset = (asset: GameAsset) => {
    const resource = Loader.shared.resources[asset];
    if (!resource) {
        throw new Error(`Resource '${asset}' not found`);
    }
    return resource;
};

export function getImage(asset: GameAsset) {
    return getAsset(asset).texture;
}

/**
 * Get image from sprite
 *
 * @param asset Sprite asset
 * @param key Image key
 */
export function getImageFromSprite(asset: GameAsset, key: string) {
    return getAsset(asset).textures![`${key}.png`];
}
