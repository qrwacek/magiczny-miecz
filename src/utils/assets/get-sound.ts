import { Loader } from 'pixi.js';

export const getSound = (key: string): Howl => {
    return Loader.shared.resources[key].data;
};
