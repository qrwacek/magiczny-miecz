import WebFont from "webfontloader";

export function loadFont(fontFamily: string) {
    return new Promise((resolve, reject) => {
        WebFont.load({
            google: {
                families: [fontFamily]
            },
            active: () => {
                resolve();
            },
            inactive: () => {
                reject();
            }
        })
    })
}
