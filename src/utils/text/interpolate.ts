export const interpolate = (template: string, values: Record<string, any>) => {
    if (typeof template !== 'string') {
        return template;
    }
    return template.replace(/{([^{}]*)}/g, (match, key) => {
        const value = values[key];

        return typeof value === 'string' || Number.isFinite(value) ? value : match;
    });
};
