import { Container, Sprite } from 'pixi.js'

export const alignRight = (element: Container | Sprite, offset: number = 0) => {
    if (!element.parent) {
        return;
    }
    element.x = element.parent.width - element.width + offset;
};

export const alignBottom = (element: Container | Sprite, offset: number = 0) => {
    if (!element.parent) {
        return;
    }
    element.y = element.parent.height - element.height + offset;
};
