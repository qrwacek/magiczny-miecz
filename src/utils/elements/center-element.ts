import { Container, Sprite } from 'pixi.js';

/**
 * Center element within its' container
 *
 * @param {Container} element Element to center
 * @param {number} offsetX Additional horizontal offset
 * @param {number} offsetY Additional vertical offset
 */
export const center = (element: Container, offsetX: number = 0, offsetY: number = 0): void => {
    centerHorizontally(element, offsetX);
    centerVertically(element, offsetY);
};

/**
 * Center element horizontally within its' container
 *
 * @param {Container} element Element to center
 * @param {number} offset Additional offset
 */
export const centerHorizontally = (element: Container, offset: number = 0) => {
    if (!element.parent) { return; }
    element.x = (element.parent.width - element.width) / 2 + offset;
};

/**
 * Center element vertically within its' container
 *
 * @param {Container} element Element to center
 * @param {number} offset Additional offset
 */
export const centerVertically = (element: Container, offset: number = 0) => {
    if (!element.parent) { return; }
    element.y = (element.parent.height - element.height) / 2 + offset;
};

/**
 * Center sprite within its' container.
 * Supports sprite anchor.
 *
 * @param {Sprite} element
 */
export const centerSprite = (element: Sprite): void => {
    centerSpriteHorizontally(element);
    centerSpriteVertically(element);
};

/**
 * Center sprite horizontally within its' container.
 * Supports sprite anchor.
 *
 * @param {Sprite} element
 */
export const centerSpriteHorizontally = (element: Sprite): void => {
    const offsetX = element.anchor.x * element.width;
    centerHorizontally(element, offsetX);
};

/**
 * Center sprite vertically within its' container.
 * Supports sprite anchor.
 *
 * @param {Sprite} element
 */
export const centerSpriteVertically = (element: Sprite): void => {
    const offsetY = element.anchor.y * element.height;
    centerVertically(element, offsetY);
};
