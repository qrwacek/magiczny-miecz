import { DisplayObject } from 'pixi.js';

export const removeElement = (element: DisplayObject): void => {
    if (element.parent) {
        element.parent.removeChild(element);
    }
    element.destroy();
};
