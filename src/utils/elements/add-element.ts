import { Container, Sprite } from 'pixi.js';

export function appendElement(
    container: Container,
    element: Container | Sprite,
    axis: 'horizontal' | 'vertical',
    gutter = 0,
) {
    const elementsCount = container.children.length;
    if (elementsCount > 0) {
        const lastElement = container.children[elementsCount - 1] as Container | Sprite;
        if (axis === 'horizontal') {
            element.x = lastElement.x + lastElement.width + gutter;
        } else {
            element.y = lastElement.y + lastElement.height + gutter;
        }
    }
    container.addChild(element);
}

export function prependElement(
    container: Container,
    element: Container | Sprite,
    axis: 'horizontal' | 'vertical',
    gutter = 0,
) {
    if (container.children.length > 0) {
        const firstElement = container.children[0] as Container | Sprite;
        if (axis === 'horizontal') {
            element.x = firstElement.x - element.width - gutter;
        } else {
            element.y = firstElement.y - element.height - gutter;
        }
    }
    container.addChild(element);
}
