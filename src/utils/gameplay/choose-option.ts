export const chooseOption = (availableOptions: string[], message: string = ''): number => {
    const nrOfOptions = availableOptions.length;
    const description = availableOptions.reduce((acc, option, index) => {
        return `${acc}\n${index + 1}. ${option}`;
    });
    const promptMessage = (message || `Select option from 1 to ${nrOfOptions}`) + description;
    let option, isCorrect;
    do {
        option = Number(prompt(promptMessage));
        isCorrect = !isNaN(option) && option >= 1 && option <= nrOfOptions;
    } while (!isCorrect);
    return option;
};
