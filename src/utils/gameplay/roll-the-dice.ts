export const rollTheDice = (nrOfDice: number = 1): number => {
    return Math.floor(Math.random() * 6 * nrOfDice) + 1;
};
