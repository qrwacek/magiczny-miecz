import { rollTheDice } from './roll-the-dice';

describe('rollTheDice', () => {
    it('should return a random number between 1 and 6 by default', () => {
        const result = rollTheDice();
        expect(result).toBeGreaterThanOrEqual(1);
        expect(result).toBeLessThanOrEqual(6);
    });
    it('should return a random number between 1 and 12 when rolling two dice', () => {
        const result = rollTheDice(2);
        expect(result).toBeGreaterThanOrEqual(1);
        expect(result).toBeLessThanOrEqual(12);
    });
});
