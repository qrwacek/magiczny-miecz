export const chooseNumber = (maxNumber: number = 100, message: string = ''): number => {
    const promptMessage = (message || 'Choose a number');
    let option, isCorrect;
    do {
        option = Number(prompt(promptMessage));
        isCorrect = !isNaN(option) && option >= 1 && option <= maxNumber;
    } while (!isCorrect);
    return option;
};
