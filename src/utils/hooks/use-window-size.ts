import { useEffect, useState } from "react";

const OFFSET_HORIZONTAL = 300;
const OFFSET_VERTICAL = 0;

export const useWindowSize = () => {
    const [[width, height, isMobile, isTablet, isDesktop], setSize] = useState(
        [0, 0, false, false, false]
    );

    useEffect(() => {
        function resizeHandler() {
            const width = window.innerWidth - OFFSET_HORIZONTAL;
            const height = window.innerHeight - OFFSET_VERTICAL;
            setSize([
                width,
                height,
                width < 768,
                width >= 768 && width < 1024,
                width >= 1024,
            ]);
        }

        window.addEventListener('resize', resizeHandler);
        resizeHandler();

        return function removeListener() {
            window.removeEventListener('resize', resizeHandler);
        }
    }, []);

    return { width, height, isMobile, isTablet, isDesktop };
}
