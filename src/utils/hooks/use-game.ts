import { useContext } from "react";
import { GameContext } from "board/board";

export function useGame() {
    const { G, ctx, moves } = useContext(GameContext);

    const currentPlayer = G.players![ctx.currentPlayer];
    const currentStage = ctx.activePlayers![ctx.currentPlayer];

    return { G, ctx, moves, currentPlayer, currentStage };
}
