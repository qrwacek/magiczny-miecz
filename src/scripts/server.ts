import { Server } from 'boardgame.io/server';
import { Game } from '../game';

const server = Server({
    games: [Game],
});

const port = +(process.env.PORT || 8000);

server.run(port, () => {
    console.log(`Game server running at http://localhost:${port}`);
});
