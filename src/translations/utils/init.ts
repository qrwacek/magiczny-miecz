import i18next from "i18next";
import { initReactI18next } from "react-i18next";

import { Language } from "../translations.const";
import { pl } from '../resources';

export function initTranslations() {
    return i18next
        .use(initReactI18next) // passes i18n down to react-i18next
        .init({
            resources: {
                [Language.PL]: {
                    translation: { ...pl }
                },
            },

            lng: Language.PL,
            fallbackLng: Language.PL,

            interpolation: {
                escapeValue: false
            }
        });
}
