import { GameMoves, GameConfig } from "boardgame.io/core";
// import { PluginPlayer, Plugin } from "boardgame.io/plugins";
import {
    IGameState,
    Moves,
    GameStage,
    PlayerId,
    IPlayerState,
    GamePhase,
} from "./game.const";
import { PluginLocations, PluginSpells } from "./plugins";
import { chooseCharacter, moveTo } from './moves';

const AllMoves: GameMoves<IGameState, Moves, PlayerId, GamePhase> = {
    explore(G, ctx) {
        console.log("explore");
        ctx.events.endStage();
    },
    pvpFight(G, ctx) {
        console.log("pvpFight");
        ctx.events.endStage();
    },
    pvpSkill(G, ctx) {
        console.log("pvpSkill");
        ctx.events.endStage();
    },
    pvpSpell(G, ctx) {
        console.log("pvpSpell");
        ctx.events.endStage();
    },
    end(G, ctx) {
        console.log("end");
        ctx.events.endStage();
        ctx.events.endTurn();
    },
};

const {
    explore,
    pvpFight,
    pvpSkill,
    pvpSpell,
    end
} = AllMoves;

export const Game: GameConfig<
    IGameState,
    IGameState,
    Moves,
    PlayerId,
    GamePhase,
    GameStage
> = {
    name: "magiczny-miecz",
    plugins: [
        // PluginPlayer as Plugin<IGameState, Moves, PlayerId, GamePhase>,
        PluginSpells,
        PluginLocations,
    ],
    seed: '12345',
    playerSetup: (playerId: PlayerId): IPlayerState => {
        return {
            name: `Player ${playerId}`
        };
    },
    setup: (ctx) => {
        const { numPlayers } = ctx;

        const initialState: IGameState = {
            start: new Date().toISOString(),
            diceRoll: 0,
            players: {}
        };

        for (let i = 0; i < numPlayers; i++) {
            initialState.players![i + ''] = {
                name: `Player ${i}`
            };
        }

        return initialState;
    },
    moves: {
        moveTo,
        explore,
        pvpFight,
        pvpSkill,
        pvpSpell,
        end,
    },
    phases: {
        characterSelect: {
            start: true,
            next: GamePhase.PLAY,
            turn: {
                activePlayers: {
                    player: GameStage.START,
                },
                stages: {
                    start: {
                        moves: { chooseCharacter },
                    },
                },
            },
            endIf: (G) => {
                if (!G.players) {
                    return false;
                }
                return Object.values(G.players).every((player) => {
                    return !!player.character
                });
            },
        },
        play: {
            turn: {
                activePlayers: {
                    player: GameStage.START,
                },
                stages: {
                    start: {
                        moves: { moveTo },
                        next: GameStage.MEET_OR_EXPLORE,
                    },
                    meetOrExplore: {
                        moves: { explore, pvpFight, pvpSkill, pvpSpell },
                        next: GameStage.END,
                    },
                    end: {
                        moves: { end },
                        next: GameStage.START,
                    }
                }
            },
        },
    },
};
