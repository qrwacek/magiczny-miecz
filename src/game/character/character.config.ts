import { ICardConfig } from '../card';
import { LocationId } from '../location';
import { ItemId } from '../item';
import { CharacterId, Nature } from './character.const';

export interface ICharacterConfig extends ICardConfig {
    might: number;
    magic: number;
    nature: Nature;
    location: LocationId;
    gold?: number;
    lifePoints?: number;
    spells?: number;
    items?: ItemId[];
    safeLocations?: LocationId[];
    canEscapeLocations?: LocationId[];
}

export const CHARACTER_CONFIG: Record<CharacterId, ICharacterConfig> = {
    [CharacterId.AWANTURNIK]: {
        name: 'Awanturnik',
        description: '1. Zamiast atakować napotkaną Postać możesz - jeśli pozwalają ci na to posiadane punktu Magii ' +
            '- zabrać jej 1 Zaklęcie. Nie możesz oglądać jej Zaklęć - wybierasz jedno losowo.\n' +
            '2. W czasie walki możesz rzucać kostką dwa razy i użyć wyższego wyniku do określenia twojej całkowitej ' +
            'liczby punktów Miecza.',
        might: 3,
        magic: 3,
        nature: Nature.CHAOTIC,
        location: LocationId.KARCZMA,
    },
    [CharacterId.BARBARZYNCA]: {
        name: 'Barbarzyńca',
        description: '1. Nie musisz rzucać kostką na Urwisku i w Wilczym Parowie (jesteś tam bezpieczny).\n' +
            '2. Na Bagnach nie stracisz Przedmiotu ani Przyjaciela, jeśli rzucając kostką uzyskasz 1 lub 2 oczka.\n' +
            '3. Jeśli zaatakujesz inną Postać (lecz nie w sytuacji, gdy sam zostałeś zaatakowany) i zwyciężysz nie ' +
            'zabierając jej 1 punktu Życia, możesz natychmiast w tej samej turze zaatakować ją po raz drugi.',
        might: 5,
        magic: 1,
        nature: Nature.CHAOTIC,
        location: LocationId.KURHAN,
        safeLocations: [LocationId.URWISKO, LocationId.WILCZY_PAROW],
    },
    [CharacterId.BLEDNY_RYCERZ]: {
        name: 'Błędny Rycerz',
        description: '1. Rozpoczynasz grę posiadając Miecz i Zbroję.\n' +
            '2. Jeśli przegrasz walkę (lecz nie walkę magiczną) możesz zignorować jej rezultat i walczyć ponownie. ' +
            'Podczas drugiej walki działać będą wszystkie Zaklęcia i Magiczne Przedmioty użyte w pierwszej walce. ' +
            'Musisz zaakceptować wynik drugiej walki.\n' +
            '3. Jeżeli w twoim posiadaniu znajdzie się Excalibur lub Święty Graal, możesz wziąć o jedną Kartę Zaklęć ' +
            'więcej niż powinieneś i odrzucić to, które ci mniej odpowiada.\n' +
            '4. Każdej napotkanej Postaci możesz odebrać Krzyżowca i Giermka.',
        might: 5,
        magic: 1,
        nature: Nature.CHAOTIC,
        location: LocationId.BEZDROZA,
        items: [ItemId.MIECZ, ItemId.ZBROJA],
    },
    [CharacterId.CZARODZIEJ]: {
        name: 'Czarodziej',
        description: '1. Rozpoczynasz grę posiadając 2 Zaklęcia.\n' +
            '2. W każdym momencie gry musisz mieć co najmniej 2 Zaklęcia. Natychmiast po rzuceniu przedostatniego, ' +
            'możesz wziąć następne.\n' +
            '3. Zdolność przewidywania przyszłości pozwala ci ignorować Nieznajomych i Spotkania, które wyciągniesz ' +
            'jako Kartę Zdarzenia.\n' +
            '4. Możesz ignorować Uroczą Diablicę',
        might: 2,
        magic: 4,
        nature: Nature.GOOD,
        location: LocationId.STUDNIA_WIECZNOSCI,
        spells: 2,
    },
    [CharacterId.DEMON]: {
        name: 'Demon',
        description: '1. Rozpoczynasz grę posiadając 1 Zaklęcie\n' +
            '2. Ignorujesz wszystkie napotkane Demony.\n' +
            '3. Masz szansę zignorować każde skierowane ku tobie Zaklęcie, jeśli rzucając kostką uzyskasz ' +
            '1 lub 2 oczka.\n' +
            '4. Jeżeli zwyciężysz inną Postać w walce magicznej i zdecydujesz się zabrać jej punkt Życia, możesz ' +
            'dodać ten punkt do swojego Życia.',
        might: 2,
        magic: 4,
        nature: Nature.EVIL,
        location: LocationId.CZARCI_MLYN,
        spells: 1,
    },
    [CharacterId.ELF]: {
        name: 'Elf',
        description: '1. Nie musisz rzucać kostką na Urwisku - jesteś tam bezpieczny.\n' +
            '2. Możesz wymykać się Wrogom na Równinach.\n' +
            '3. Jeżel kończysz ruch na Równinie, możesz natychmiast iść dalej - zyskujesz dodatkowy ruch. W takim ' +
            'wypadku nie musisz wypełniać instrukcji związanej z Równiną, na której zakończyłeś ruch i spotykać ' +
            'znajdujące się tam Postacie. Dodatkowy ruch możesz wykorzystać tylko raz na turę.',
        might: 3,
        magic: 4,
        nature: Nature.GOOD,
        location: LocationId.UROCZYSKO,
        canEscapeLocations: [LocationId.ROWNINA_SAMOTNYCH_SKAL, LocationId.ROWNINA_SNU, LocationId.ROWNINA_TRAW],
    },
    [CharacterId.GOBLIN]: {
        name: 'Goblin',
        description: '1. Nie musisz rzucać kostką w Kręgu Mocy i Wilczym Parowie - jesteś tam bezpieczny.\n' +
            '2. Jeżeli spotkasz Nobbina, możesz go wziąć jako swojego Przyjaciela - podczas walki będzie dodawał ' +
            'swoje punkty Miecza do twoich.\n' +
            '3. Napotkanego Hadrona możesz przesunąć na dowolny, nie zajęty przez inną Postać, Obszar w Kręgu, ' +
            'po którym wędrujesz. Niezależnie od tego, czy go przesuniesz, czy nie, Hadron nie będzie cię atakował ' +
            '(lecz ty możesz go zaatakować).',
        might: 3,
        magic: 3,
        nature: Nature.CHAOTIC,
        location: LocationId.MOKRADLA,
        safeLocations: [LocationId.KRAG_MOCY, LocationId.WILCZY_PAROW],
    },
    [CharacterId.HOBGOBLIN]: {
        name: 'Hobgoblin',
        description: '1. Nie musisz rzucać kostką przy Kurhanie i w Krypcie Upiorów - jesteś tam bezpieczny.\n' +
            '2. Możesz wymykać się Wrogom na Stepie.\n' +
            '3. Określając Miecz Kamiennego Potwora w Ruinach Twierdzy, możesz odjąć 1 od wyniku rzutu.\n' +
            '4. Jeżeli spotkasz Hadrona, możesz go wziąć jako swojego Przyjaciela - podczas walki będzie dodawał ' +
            'swoje punkty Miecza do twoich.',
        might: 3,
        magic: 3,
        nature: Nature.EVIL,
        location: LocationId.STEP,
        safeLocations: [LocationId.KURHAN, LocationId.KRYPTA_UPIOROW],
        canEscapeLocations: [LocationId.STEP],
    },
    [CharacterId.HUMMIT]: {
        name: 'Hummit',
        description: '1. Rozpoczynasz grę posiadając 1 Zaklęcie.\n' +
            '2. Nie musisz rzucać kostką w Kręgu Mocy i na Urwisku - jesteś tam bezpieczny.\n' +
            '3. Możesz dodać 1 do wyniku rzutu kostką podczas rozgrywania walk na Stepie i w Dolinach.\n' +
            '4. Jeżeli chcesz możesz zignorować Zaklętą Ścieżkę.\n' +
            '5. Możesz wziąć Wilka lub Łosia za Przyjaciela (lecz nie oba zwierzęta na raz) i dosiąść ich. W takim ' +
            'wypadku rzucasz dwoma kostkami określając ruch.',
        might: 2,
        magic: 3,
        nature: Nature.GOOD,
        location: LocationId.BEZDROZA,
        spells: 1,
        safeLocations: [LocationId.KRAG_MOCY, LocationId.URWISKO],
    },
    [CharacterId.KAPLAN]: {
        name: 'Kapłan',
        description:
            '1. Rozpoczynasz grę posiadając 1 Zaklęcie.\n' +
            '2. Podczas modlitw możesz powtórzyć rzut kostką, jeśli wynik pierwszego rzutu cię nie zadowoli '+
            '(wynik drugiego rzutu musisz zaakceptować)\n' +
            '3 W spotkaniach z Demonami możesz zamiast walki próbować pokonać je przy pomocy egzorcyzmów. ' +
            'W takim przypadku wykonujesz rzut kostką: gdy wynikiem jest 1,2 lub 3, zwyciężasz. ' +
            'Jeżeli wynik jest inny, Demon atakuje cię w zwykły sposób.',
        might: 2,
        magic: 4,
        nature: Nature.CHAOTIC,
        location: LocationId.KRAG_MOCY,
        spells: 1,
    },
    [CharacterId.KAPLANKA]: {
        name: 'Kapłanka',
        description:
            '1. Rozpoczynasz grę posiadając 2 Zaklęcia.\n' +
            '2. W każdym momencie gry musisz posiadać conajmniej 2 Zaklęcia - możesz wziąć następne natychmiast ' +
            'po rzuceniu przedostatniego.\n' +
            '3. Nie atakują cię Bestie (lecz ty możesz je atakować). Napotkany Łoś, Wilk lub Niedźwiedź stanie się ' +
            'twoim Przyjacielem i będzie dodawał swój Miecz do twojego w walce, jeżeli rzucając kostką ' +
            'uzyskasz 1,2 lub 3 oczka. ',
        might: 1,
        magic: 5,
        nature: Nature.GOOD,
        location: LocationId.UROCZYSKO,
        spells: 2,
    },
    [CharacterId.KARZEL]: {
        name: 'Karzeł',
        description:
            '1. Rozpoczynasz grę posiadając 2 Zaklęcia.\n' +
            '2. W każdym momencie gry musisz posiadać co najmniej 2 Zaklęcia (możesz wziąć następne po rzuceniu przedostatniego).\n' +
            '3. Nie musisz płacić za przejście Strażnikowi Magicznych Wrót.',
        might: 2,
        magic: 5,
        nature: Nature.EVIL,
        location: LocationId.MOKRADLA,
        spells: 2,
        safeLocations: [LocationId.STRAZNIK_MAGICZNYCH_WROT],
    },
    [CharacterId.KAT]: {
        name: 'Kat',
        description:
            '1. Swój charakter określasz sam na początku rozgrywki. Możesz to zrobić dopiero gdy wszyscy inni ' +
            'uczestnicy wybiorą swoje Postacie.\n' +
            '2. Rozpoczynasz grę posiadając 1 Zaklęcie i Miecz.\n' +
            '3. Atakując inną Postać (lecz nie w sytuacji, gdy sam zostałeś zaatakowany) możesz próbować ' +
            'ściąć jej głowę. Uda ci się to, gdy w rzucie kostką uzyskasz 1 lub 2 oczka. ' +
            'Jeżeli ci się nie powiedzie, masz obowiązek kontynuować walkę w zwykły sposób.\n' +
            '4. Atakując Postać (lecz nie jeśli to ona atakuje ciebie) możesz wybrać rodzaj walki - zwykłą lub magiczną.',
        might: 2,
        magic: 4,
        nature: Nature.CHAOTIC,
        location: LocationId.GROD,
        spells: 1,
        items: [ItemId.MIECZ],
    },
    [CharacterId.KRASNOLUD]: {
        name: 'Krasnolud',
        description:
            '1. Rozpoczynasz grę posiadając Tarczę i Sztylet.\n' +
            '2. W każdej sytuacji, gdy wykonujesz rzut kostką, możesz zignorować pierwszy wynik i rzucić ponownie. ' +
            'Musisz jednak zaakceptować wynik drugiego rzutu.\n' + 
            '3. Możesz zignorować każde, rzucone na ciebie Zaklęcie, jeśli rzucając kostką uzyskasz 1 lub 2 oczka.',
        might: 3,
        magic: 3,
        nature: Nature.CHAOTIC,
        location: LocationId.OSADA,
        items: [ItemId.TARCZA, ItemId.SZTYLET],
    },
    [CharacterId.KSIAZE]: {
        name: 'Książę',
        description:
            '1. Rozpoczynasz grę mając 5 Sztuk Złota.\n' +
            '2. Rozpoczynasz grę posiadając Hełm i Miecz. Jeżeli je w jakikolwiek sposób utracisz, ' +
            'możesz je zawsze odzyskać odwiedzając Zamek.\n' +
            '3. Masz prawo nie badać Obszaru, na którym się znajdziesz, jednak - jeśli wymaga tego instrukcja - ' +
            'musisz wyciągnąć Karty Zdarzeń. Jeżeli nie chcesz badać Obszaru, nie oglądasz ich ' +
            'i zostawiasz zakryte na planszy.',
        might: 4,
        magic: 3,
        nature: Nature.CHAOTIC,
        location: LocationId.GROD,
        gold: 5,
        items: [ItemId.HELM, ItemId.MIECZ],
    },
    [CharacterId.LOTR]: {
        name: 'Łotr',
        description:
            '1. Rozpoczynasz grę posiadając Sztylet.\n' +
            '2. Nie możesz mieć żadnych Przyjaciół.\n' +
            '3. Ilekroć pokonasz inną Postać, możesz odebrać jej 1 punkt Miecza albo Magii zamiast punktu Życia. ' +
            'Zabranych punktów nie możesz dodać do swoich. Nie możesz tak postąpić jeżeli walczyłeś nieuczciwie.\n' +
            '4. Jeżeli atakując inną Postać zadeklarujesz, że walczysz nieuczciwie, przeciwnik określając swój ' +
            'całkowity Miecz, nie dodaje do niego wyniku rzutu kostką. Nie możesz tak postępować, jeśli sam zostałeś ' +
            'zaatakowany.',
        might: 5,
        magic: 1,
        nature: Nature.EVIL,
        location: LocationId.KARCZMA,
        items: [ItemId.SZTYLET],
    },
    [CharacterId.MAG]: {
        name: 'Mag',
        description:
            '1. Rozpoczynasz grę posiadając 2 Zaklęcia.\n' +
            '2. W każdym momencie gry musisz posiadać co najmniej 1 Zaklęcie - natychmiast po rzuceniu ostatniego ' +
            'możesz wziąć następne.\n' +
            '3. Nie atakuje cię Golem ani Homunculus.\n' +
            '4. Możesz wskrzesić pokonaną Bestię, która stanie się twoim Przyjacielem i podczas jednej walki doda ' +
            'swoje punkty do twoich. Po jednej walce nieodwracalnie umiera - musisz odłożyć jej Kartę (nie możesz ' +
            'wymienić jej na dodatkowe punkty Miecza).',
        might: 2,
        magic: 5,
        nature: Nature.CHAOTIC,
        location: LocationId.KRAG_MOCY,
        spells: 2,
    },
    [CharacterId.MAGOG]: {
        name: 'Magog',
        description:
            '1. Rozpoczynasz grę posiadając 1 Zaklęcie.\n' +
            '2. Możesz dowolnie zmieniać swoją naturę, jednak musi być ona określona w każdym momencie gry ' +
            '(na przykład: posiadasz Świętą Włócznię, a masz zamiar zmienić naturę na złą, będziesz musiał odrzucić ' +
            'Przedmiot Magiczny i pozostawić go tam, gdzie nastąpi zmiana twojej natury).\n' +
            '3. Możesz dodać 1 do wyniku rzutu kostką w walkach rozgrywanych na Równinach.\n' +
            '4. Możesz ignorować Mgłę.',
        might: 3,
        magic: 4,
        nature: Nature.CHAOTIC,
        location: LocationId.STEP,
        spells: 1,
    },
    [CharacterId.OBBOL]: {
        name: 'Obbol',
        description:
            '1. Możesz wymykać się Wrogom na Mokradłach.\n' +
            '2. Określając Magię Ducha Skał w Wymarłym Mieście możesz odjąć 1 od wyniku rzutu kostką.\n' +
            '3. Ponieważ jesteś wyjątkowo gruboskórny, możesz zostać pokonany (w obu rodzajach walki) różnicą ' +
            'przynajmniej 2 punktów (przy różnicy jednopunktowej wynik walki jest nie rozstrzygnięty).',
        might: 2,
        magic: 3,
        nature: Nature.CHAOTIC,
        location: LocationId.MOKRADLA,
        canEscapeLocations: [LocationId.MOKRADLA],
    },
    [CharacterId.OLBRZYM]: {
        name: 'Olbrzym',
        description:
            '1. Ze względu na swój wzrost, twoje kroki są o wiele dłuższe, niż kroki innych - jeśli twój ruch kończy ' +
            'się na Obszarze zajętym przez inną Postać, możesz przejść ten Obszar i zatrzymać się na pierwszym wolnym.\n' +
            '2. W czasie walki z Postacią, której całkowity Miecz przewyższa twój całkowity Miecz o wyniku starcia ' +
            'decyduje jedynie wynik rzutu kostką.\n' +
            '3. Ponieważ patrzysz na wszystko z wysoka, ciągnąc Karty Zdarzeń możesz wziąć o jedną więcej niż zaleca ' +
            'instrukcja i odrzucić tę, która ci nie odpowiada.',
        might: 4,
        magic: 2,
        nature: Nature.CHAOTIC,
        location: LocationId.STEP,
    },
    [CharacterId.PUSTELNIK]: {
        name: 'Pustelnik',
        description:
            '1. Podczas walki (lecz nie walki magicznej) możesz dodawać do punktów Miecza punkty Magii.\n' +
            '2. Nie możesz używać Miecza, Sztyletu, Hełmu ani Zbroi.\n' +
            '3. Zamiast walczyć z napotkanym Demonem, możesz go odpędzić słowami "Zgiń, przepadnij!". Przesuwasz ' +
            'wtedy jego Kartę na dowolny Obszar Kręgu, po którym wędrujesz, nie zajęty przez inną Postać.',
        might: 2,
        magic: 3,
        nature: Nature.GOOD,
        location: LocationId.UROCZYSKO,
    },
    [CharacterId.QUARK]: {
        name: 'Quark',
        description:
            '1. Rozpoczynasz grę posiadając 1 Zaklęcie.\n' +
            '2. Zamiast atakować spotkaną Postać, możesz rzucić na nią urok. W następnej turze ofiara nie może nic ' +
            'zrobić, ty zaś wykorzystujesz jej ruch (nie możesz korzystać z tej umiejętności na Kamiennym Moście).\n' +
            '3. Jeśli w rzucie dotyczącym ruchu uzyskasz 6 oczek, możesz przenieść się do dowolnego Obszaru w Kręgu, ' +
            'po którym podróżujesz.',
        might: 3,
        magic: 3,
        nature: Nature.CHAOTIC,
        location: LocationId.MROZNE_PUSTKOWIE,
        spells: 1,
    },
    [CharacterId.RYCERZ_CIEMNOSCI]: {
        name: 'Rycerz Ciemności',
        description:
            '1. Rozpoczynasz grę posiadając 1 Zaklęcie i Miecz.\n' +
            '2. Atakując możesz wybrać formę walki (zwykłą lub magiczną) - nie masz takiej możliwości, gdy sam ' +
            'zostaniesz zaatakowany.\n' +
            '3. Za każde 10 punktów Magii pokonanych przez ciebie Demonów zyskujesz do wyboru: 1 Zaklęcie lub ' +
            '1 punkt Magii.\n' +
            '4. Podczas Turnieju Rycerskiego możesz stoczyć dwa pojedynki, pod warunkiem, że wygrasz pierwszą walkę.',
        might: 3,
        magic: 3,
        nature: Nature.EVIL,
        location: LocationId.CZARCI_MLYN,
        spells: 1,
        items: [ItemId.MIECZ],
    },
    [CharacterId.SPRYCIARZ]: {
        name: 'Spryciarz',
        description:
            '1. Od napotkanej Postaci możesz próbować wyłudzić 1 Sz. Z. Sztuczka uda się jeśli wyrzucisz kostką 1.\n' +
            '2. Jeżeli inna Postać rozpoczyna ruch z Obszaru, na którym się znajdujesz, możesz jej towarzyszyć ' +
            'w drodze i zakończyć ruch na Obszarze, na którym i ona go zakończy. Nie możesz badać tego Obszaru, ' +
            'a podczas swojej kolejki poruszasz się w zwykły sposób.\n' +
            '3. Jeżeli nie atakujesz Bestii, możesz je oswoić. Bestia staje się twoim Przyjacielem i podczas walki ' +
            'dodaje swój Miecz do twojego. W trakcie gry możesz oswoić tylko 1 Bestię.',
        might: 4,
        magic: 3,
        nature: Nature.GOOD,
        location: LocationId.GROD,
    },
    [CharacterId.TROLL]: {
        name: 'Troll',
        description:
            '1. Nie musisz rzucać kostką w Kręgu Mocy i przy Kurhanie - jesteś tam bezpieczny.\n' +
            '2. Możesz przepędzić zwyciężoną Postać z Obszaru, na którym toczyła się walka. Rzuć kostką i po ' +
            'zaaplikowaniu pokonanemu kary wynikającej z przegranej walki, przesuń go w dowolnym kierunku o liczbę ' +
            'Obszarów zgodną z liczbą wyrzuconych oczek. Nie możesz tego zrobić na Kamiennym Moście.',
        might: 6,
        magic: 1,
        nature: Nature.CHAOTIC,
        location: LocationId.MROZNE_PUSTKOWIE,
        safeLocations: [LocationId.KRAG_MOCY, LocationId.KURHAN],
    },
    [CharacterId.WIEDZMA]: {
        name: 'Wiedźma',
        description:
            '1. Rozpoczynasz grę posiadając 1 Zaklęcie.\n' +
            '2. Możesz rzucić urok na napotkaną Postać lub z nią walczyć (nigdy jedno i drugie). Ofiara musi jak ' +
            'najszybciej, lecz w zwykły sposób udać się do świątyni Nemed albo Świątyni Tolimana, gdzie urok ' +
            'zostanie z niej zdjęty. Dopiero później będzie mogła kontynuować wędrówkę.',
        might: 2,
        magic: 4,
        nature: Nature.EVIL,
        location: LocationId.GROD,
        spells: 2,
    },
    [CharacterId.ZDOBYWCA]: {
        name: 'Zdobywca',
        description:
            '1. Rozpoczynasz grę posiadając Miecz i Tarczę.\n' +
            '2. Zawsze wolno ci wymienić Kartę Przyjaciela na 1 punkt Życia.\n' +
            '3. Po wygraniu zwykłej walki możesz odebrać pokonanej Postaci 2 punkty Życia.',
        might: 4,
        magic: 3,
        nature: Nature.CHAOTIC,
        location: LocationId.OSADA,
        items: [ItemId.MIECZ, ItemId.TARCZA],
    },
};
