import { CharacterId, DEFAULT_GOLD_AMOUNT, DEFAULT_LIFE_POINTS, Nature, ICharacterAction } from './character.const';
import { CHARACTER_CONFIG } from './character.config';
import { LocationId } from '../location';
import { Card } from '../card';
import { ItemId } from '../item';
import { GameStage } from "../game.const";

export class Character extends Card {
    public might: number;
    public magic: number;
    public gold: number;
    public life: number;

    public nature: Nature;
    public location: LocationId;

    public spells?: number;
    public items?: ItemId[];

    protected safeLocations?: LocationId[];
    protected canEscapeLocations?: LocationId[];

    protected actions?: ICharacterAction[];

    public constructor(id: CharacterId) {
        const config = CHARACTER_CONFIG[id];

        super(id, config);

        this.might = config.might;
        this.magic = config.magic;
        this.gold = config.gold || DEFAULT_GOLD_AMOUNT;
        this.life = config.lifePoints || DEFAULT_LIFE_POINTS;

        this.nature = config.nature;
        this.location = config.location;

        this.spells = config.spells;
        this.items = config.items;

        this.safeLocations = config.safeLocations;
        this.canEscapeLocations = config.canEscapeLocations;
    }

    public async isSafeInLocation(location: LocationId) {
        if (!this.safeLocations) {
            return false;
        }
        return this.safeLocations.includes(location);
    }

    public async canEscapeInLocation(location: LocationId) {
        if (!this.canEscapeLocations) {
            return false;
        }
        return this.canEscapeLocations.includes(location);
    }

    public getAvailableActions(stage?: GameStage): ICharacterAction[] {
        if (!this.actions) {
            return [];
        }
        if (typeof stage === 'undefined') {
            return this.actions;
        }
        return this.actions.filter((action) => action.stage === stage);
    }

    public implementsAction(action: ICharacterAction) {
        if (!this.actions) {
            return false;
        }
        return this.actions.some((item) => item.id === action.id);
    }

    /**
     * Action router. Should be overridden by specific character
     *
     * @param action Action to be performed
     */
    public async performAction(action: ICharacterAction) {
        if (!this.implementsAction(action)) {
            throw new Error(`Action ${action} not implemented by ${this.name}`);
        }
    }
}
