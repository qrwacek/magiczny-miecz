import { GameStage } from "../game.const";

export enum Nature {
    GOOD = 'good',
    CHAOTIC = 'chaotic',
    EVIL = 'evil',
}

export enum CharacterId {
    AWANTURNIK = 'awanturnik',
    BARBARZYNCA = 'barbarzynca',
    BLEDNY_RYCERZ = 'bledny-rycerz',
    CZARODZIEJ = 'czarodziej',
    DEMON = 'demon',
    ELF = 'elf',
    GOBLIN = 'goblin',
    HOBGOBLIN = 'hobgoblin',
    HUMMIT = 'hummit',
    KAPLAN = 'kaplan',
    KAPLANKA = 'kaplanka',
    KARZEL = 'karzel',
    KAT = 'kat',
    KRASNOLUD = 'krasnolud',
    KSIAZE = 'ksiaze',
    LOTR = 'lotr',
    MAG = 'mag',
    MAGOG = 'magog',
    OBBOL = 'obbol',
    OLBRZYM = 'olbrzym',
    PUSTELNIK = 'pustelnik',
    QUARK = 'quark',
    RYCERZ_CIEMNOSCI = 'rycerz-ciemnosci',
    SPRYCIARZ = 'spryciarz',
    TROLL = 'troll',
    WIEDZMA = 'wiedzma',
    ZDOBYWCA = 'zdobywca',
}

export interface ICharacterAction {
    id: string;
    name: string;
    stage: GameStage;
}

export const ALL_CHARACTERS = [
    CharacterId.AWANTURNIK, CharacterId.BARBARZYNCA, CharacterId.BLEDNY_RYCERZ, CharacterId.CZARODZIEJ,
    CharacterId.DEMON, CharacterId.ELF, CharacterId.GOBLIN, CharacterId.HOBGOBLIN, CharacterId.HUMMIT,
    CharacterId.KAPLAN, CharacterId.KAPLANKA, CharacterId.KARZEL, CharacterId.KAT, CharacterId.KRASNOLUD,
    CharacterId.KSIAZE, CharacterId.LOTR, CharacterId.MAG, CharacterId.MAGOG, CharacterId.OBBOL,
    CharacterId.OLBRZYM, CharacterId.PUSTELNIK, CharacterId.QUARK, CharacterId.RYCERZ_CIEMNOSCI,
    CharacterId.SPRYCIARZ, CharacterId.TROLL, CharacterId.WIEDZMA, CharacterId.ZDOBYWCA,
];

export const DEFAULT_LIFE_POINTS = 4;
export const DEFAULT_GOLD_AMOUNT = 1;
