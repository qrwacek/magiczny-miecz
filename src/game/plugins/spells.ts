import { ALL_SPELLS, SPELL_NUMBER, SpellId } from "game/spell";
import { GamePlugin } from "../game.const";

/**
 * Plugin that maintains state for spells in G.spells.
 */
export const PluginSpells: GamePlugin = {
    G: {
        setup: (G, ctx) => {
            const spells: SpellId[] = [];

            ALL_SPELLS.forEach((spell) => {
                spells.push(
                    ...(Array(SPELL_NUMBER[spell] || 1).fill(spell))
                );
            });

            return { ...G, spells: ctx.random.Shuffle(spells) };
        },
    },
}
