import { LocationId, LOCATIONS_BY_REGION, Region } from "../location";
import { ILocationState, GamePlugin } from "../game.const";

function createRegionState(region: Region): ILocationState[] {
    const locationIndexes: Partial<Record<LocationId, number>> = {};

    return LOCATIONS_BY_REGION[region].map((locationId) => {
        const locationIndex = locationIndexes[locationId] || 0;
        locationIndexes[locationId] = locationIndex + 1;

        return {
            id: locationId,
            index: locationIndex,
            events: [],
            items: [],
            spells: [],
            gold: 0,
        };
    });
}

/**
 * Plugin that maintains state for each location in G.locations.
 * During a turn, G.location will contain the object for the current location.
 */
export const PluginLocations: GamePlugin = {
    G: {
        setup: (G) => {
            const locations = [
                ...createRegionState(Region.BOTTOM),
                ...createRegionState(Region.MIDDLE),
                ...createRegionState(Region.UPPER),
                ...createRegionState(Region.BRIDGE),
            ];

            return { ...G, locations };
        },
    },
}
