export class Attribute {
    /**
     * Current value of an attribute
     */
    protected value: number;

    public constructor(
        /**
         * Attribute initial value
         */
        protected initialValue: number,
        /**
         * Set to true if attribute value should not fall below initial value
         */
        protected minIsInitial: boolean = false, // TODO: find better name
    ) {
        this.value = initialValue;
    }

    /**
     * Increase attribute value
     *
     * @param nrOfPoints Number of points
     * @returns New value of an attribute
     */
    public gain(nrOfPoints: number = 1): number {
        return this.value += nrOfPoints;
    }

    /**
     * Decrease attribute value
     *
     * @param nrOfPoints Number of points
     * @returns Number of points lost or false if unable to lose any more points
     */
    public forfeit(nrOfPoints: number = 1): boolean | number {
        if (this.minIsInitial) {
            if (this.value === this.initialValue) {
                return false;
            } else if (this.value - nrOfPoints < this.initialValue) {
                this.value = this.initialValue;
                return nrOfPoints - this.initialValue;
            }
        }
        if (this.value < nrOfPoints) {
            const pointsLost = this.value;
            this.value = 0;
            return pointsLost;
        }
        this.value -= nrOfPoints;
        return nrOfPoints;
    }

    public get points() {
        return this.value;
    }
}
