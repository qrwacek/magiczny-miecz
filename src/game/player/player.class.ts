import { EventEmitter } from 'eventemitter3';

import { interpolate } from '@utils/text';
import { Location, Region, LocationId } from '../location';
import { Character, Nature } from '../character';
import { Item, ItemId, createItem } from '../item';
import { Log } from '@app/log';
import { Enemy } from '../enemy';
import { Gameplay } from '../gameplay';
import { EventSet, ItemSet, SpellSet } from '@app/card';

import { MoveDirection } from './player.const';
import { Fight } from '@app/gameplay/fight';

import { IPlayerState } from '../game.const';

/**
 * Player
 *
 * @emits move Emitted when player moved to a new location. Provides new location
 * @emits life-loss Emitted when player lost life during battle or other event
 * @emits round-loss Emitted when player lost round during event
 */
export class Player extends EventEmitter {
    public name: string;
    public character: Character;

    public nature: Nature;
    public moveDirection: MoveDirection = MoveDirection.CLOCKWISE;
    public location: LocationId;

    public items: ItemSet;
    public spells: SpellSet;
    public events: EventSet;

    public rounds: number = 0;
    public petrifiedRounds: number = 0;

    protected isDead: boolean = false;

    protected enemiesMight: number = 0;
    protected enemiesMagic: number = 0;

    public constructor(
        protected player: IPlayerState,
        public gameplay: Gameplay,
    ) {
        super();

        this.name = player.name;
        this.character = new Character(player.character!);

        this.nature = player.nature!;
        this.location = player.location!;

        this.items = new ItemSet();
        if (player.items) {
            player.items.forEach((itemId) => {
                this.items.put(createItem(itemId));
            });
        }

        this.spells = new SpellSet();
        if (player.spells) {
            player.spells.forEach((spellId) => {
                // this.spells.put(new Spell(spellId));
            });
        }

        this.events = new EventSet();
        if (player.events) {
            player.events.forEach((eventId) => {
                // this.events.put(new Event(eventId));
            });
        }
    }

    public async rollTheDice(nrOfDice: number = 1) {
        const result = await this.gameplay.rollTheDice(nrOfDice);
        this.logInfo('Wyrzucasz kostką {result}', { result });
        return result;
    }

    public async chooseOption(availableOptions: string[], message: string = ''): Promise<number> {
        const selectedOption = await this.gameplay.chooseOption(availableOptions, message);
        this.logInfo('{player} wybiera opcję "{option}"', {
            option: availableOptions[selectedOption - 1],
        });
        return selectedOption;
    }

    public get might() {
        return this.player.might!;
    }

    public set might(might: number) {
        this.player.might = might;
    }

    public gainMight(nrOfPoints: number = 1) {
        this.might += nrOfPoints;
        this.showNotification('{player} zyskuje {nrOfPoints} pkt. miecza', { nrOfPoints });
    }

    public forfeitMight(nrOfPoints: number = 1) {
        const pointsLost = this.might >= nrOfPoints ? nrOfPoints : nrOfPoints - this.might;
        if (pointsLost) {
            this.might -= pointsLost;
            this.showNotification('{player} traci {points} pkt. miecza', { points: pointsLost });
        } else {
            this.showNotification('{player} nie może utracić więcej punktów miecza.');
        }
    }

    public getTotalMight(forFight: boolean = false): number {
        let totalMight = this.character.might + this.might;
        if (forFight) {
            // TODO: add might points from items and friends
        }
        return totalMight;
    }

    public get magic() {
        return this.player.magic!;
    }

    public set magic(magic: number) {
        this.player.magic = magic;
    }

    public gainMagic(nrOfPoints: number = 1) {
        this.magic += nrOfPoints;
        this.showNotification('{player} zyskuje {nrOfPoints} pkt. magii', { nrOfPoints });
    }

    public forfeitMagic(nrOfPoints: number = 1) {
        const pointsLost = this.magic >= nrOfPoints ? nrOfPoints : nrOfPoints - this.magic;
        if (pointsLost) {
            this.magic -= pointsLost;
            this.showNotification('{player} traci {points} pkt. magii', { points: pointsLost });
        } else {
            this.showNotification('{player} nie może utracić więcej punktów magii.');
        }
    }

    public getTotalMagic(forFight: boolean = false): number {
        const totalMagic = this.character.magic + this.magic;
        if (forFight) {
            // TODO: add magic points from magic items and friends
        }
        return totalMagic;
    }

    public get gold() {
        return this.player.gold!;
    }

    public set gold(gold: number) {
        this.player.gold = gold;
    }

    public gainGold(amount: number = 1) {
        this.gold += amount;
        this.showNotification('{player} zyskuje {amount} Sz. Z.', { amount });
    }

    public forfeitGold(amount: number = 1): boolean | number {
        const amountLost = this.gold >= amount ? amount : amount - this.gold;
        if (amountLost) {
            this.gold -= amountLost;
            this.showNotification('{player} traci {amount} Sz. Z.', { amountLost });
        } else {
            this.showNotification('{player} nie posiada już złota');
        }
        return amountLost;
    }

    public spendGold(amount: number = 1) {
        if (this.gold >= amount) {
            this.gold -= amount;
            this.showNotification('{player} wydaje {amount} sz. z.', { amount });
        } else {
            this.logError('Brak wystarczającej ilości sztuk złota');
        }
    }

    public get life() {
        return this.player.life!;
    }

    public set life(life: number) {
        this.player.life = life;
    }

    public gainLife(nrOfPoints: number = 1) {
        this.life += nrOfPoints;
        this.showNotification('{player} zyskuje {nrOfPoints} pkt. życia', { nrOfPoints });
    }

    public forfeitLife(nrOfPoints: number = 1) {
        const pointsLost = this.life >= nrOfPoints ? nrOfPoints : nrOfPoints - this.life;
        this.showNotification('{player} traci {points} pkt. życia', { points: pointsLost });
        this.emit('life-loss');

        if (this.life === 0) {
            this.die();
        }
    }

    public die() {
        this.showNotification('{player} umiera');
        this.player.isDead = true;
    }

    public buyLifePoints(nrOfPoints: number, unitPrice: number) {
        const totalPrice = unitPrice * nrOfPoints;

        if (this.gold < totalPrice) {
            return this.showNotification('{player} nie posiada wystarczającej ilości Sz. Z.');
        }

        this.gainLife(nrOfPoints);
        this.spendGold(totalPrice);
    }

    public async fightWith(enemy: Enemy) {
        await this.showInfo('Rozpoczynasz walkę z przeciwnikiem {enemy}', { enemy: enemy.info }, {
            title: 'Walka',
            confirmationText: 'Rozpocznij',
        });

        if (enemy.usesMight) {
            let playerMight = this.getTotalMight(true);
            if (!enemy.isImmuneToWeapons && this.items.weapons.length) {
                const selectedWeapon = await this.selectWeapon('Wybierz broń, której chcesz użyć w walce');
                playerMight += selectedWeapon.might;
            }
            await this.showInfo(
                'Całkowity miecz postaci wynosi: {playerMight}.\n\n' +
                'Rzuć kostką - uzyskany wynik zostanie dodany do Twoich punktów miecza',
                { playerMight },
                { title: 'Walka', confirmationText: 'Rzuć kostką' },
            );
            const diceRoll = await this.rollTheDice();
            await this.showInfo(
                'Rzuć ponownie kostką - uzyskany wynik zostanie dodany do punktów miecza przeciwnika',
                {},
                { title: 'Walka', confirmationText: 'Rzuć kostką' },
            );
            const enemyDiceRoll = await this.rollTheDice();

            const playerScore = playerMight + diceRoll;
            const enemyScore = enemy.powerPoints + enemyDiceRoll;
            if (playerScore > enemyScore) {
                await this.showInfo(
                    'Wygrywasz walkę.\n\n' +
                    'Twój wynik: {playerScore}\n' +
                    'Wynik przeciwnika: {enemyScore}',
                    { playerScore, enemyScore },
                    { title: 'Wygrana', confirmationText: 'Kontynuuj' },
                );
                this.enemiesMight += enemy.powerPoints;
            } else if (playerScore < enemyScore) {
                await this.showInfo(
                    'Przegrywasz walkę.\n\n' +
                    'Twój wynik: {playerScore}\n' +
                    'Wynik przeciwnika: {enemyScore}',
                    { playerScore, enemyScore },
                    { title: 'Przegrana', confirmationText: 'Kontynuuj' },
                );
                this.forfeitLife();
            } else {
                await this.showInfo(
                    'Walka pozostaje nierozstrzygnięta.\n\n' +
                    'Twój wynik: {playerScore}\n' +
                    'Wynik przeciwnika: {enemyScore}',
                    { playerScore, enemyScore },
                    { title: 'Remis', confirmationText: 'Kontynuuj' },
                );
            }
        }

        if (enemy.usesMagic) {
            const playerMagic = this.getTotalMagic(true);
            await this.showInfo(
                'Całkowita magia postaci wynosi: {playerMagic}.\n\n' +
                'Rzuć kostką - uzyskany wynik zostanie dodany do Twoich punktów magii',
                { playerMagic },
                { title: 'Walka magiczna', confirmationText: 'Rzuć kostką' },
            );
            const diceRoll = await this.rollTheDice();
            await this.showInfo(
                'Rzuć ponownie kostką - uzyskany wynik zostanie dodany do punktów magii przeciwnika',
                {},
                { title: 'Walka magiczna', confirmationText: 'Rzuć kostką' },
            );
            const enemyDiceRoll = await this.rollTheDice();

            const playerScore = playerMagic + diceRoll;
            const enemyScore = enemy.powerPoints + enemyDiceRoll;
            if (playerScore > enemyScore) {
                await this.showInfo(
                    'Wygrywasz walkę.\n\n' +
                    'Twój wynik: {playerScore}\n' +
                    'Wynik przeciwnika: {enemyScore}',
                    { playerScore, enemyScore },
                    { title: 'Wygrana', confirmationText: 'Kontynuuj' },
                );
                this.enemiesMagic += enemy.powerPoints;
            } else if (playerScore < enemyScore) {
                await this.showInfo(
                    'Przegrywasz walkę.\n\n' +
                    'Twój wynik: {playerScore}\n' +
                    'Wynik przeciwnika: {enemyScore}',
                    { playerScore, enemyScore },
                    { title: 'Przegrana', confirmationText: 'Kontynuuj' },
                );
                this.forfeitLife();
            } else {
                await this.showInfo(
                    'Walka pozostaje nierozstrzygnięta.\n\n' +
                    'Twój wynik: {playerScore}\n' +
                    'Wynik przeciwnika: {enemyScore}',
                    { playerScore, enemyScore },
                    { title: 'Remis', confirmationText: 'Kontynuuj' },
                );
            }
        }
    }

    /**
     * Gain spells if possible. This requires magic points
     *
     * @param nrOfSpells Requested nr of spells
     * @returns Number of spells added or false if not able to gain more spells
     */
    public gainSpell(nrOfSpells: number = 1): boolean | number {
        if (this.spells.length >= this.nrOfAllowedSpells) {
            this.showNotification('{player} nie może posiadać więcej Zaklęć');
            return false;
        }

        const spellsToDraw = nrOfSpells - (this.nrOfAllowedSpells - this.spells.length);

        this.gameplay.spells.draw(spellsToDraw).forEach((spell) => {
            this.spells.put(spell);
            this.showNotification('{player} otrzymuje zaklęcie {spell}', {
                spell: spell.name,
            });
        });
        return spellsToDraw;
    }

    public get nrOfAllowedSpells(): number {
        const magic = this.getTotalMagic();
        if (magic > 4) { return 3; }
        if (magic > 2) { return 2; }
        if (magic > 1) { return 1; }
        return 0;
    }

    public async selectItem(title?: string) {
        return this.gameplay.selectCard(this.items.all, title || 'Wybierz przedmiot', true);
    }

    public async selectWeapon(title?: string) {
        return this.gameplay.selectCard(this.items.weapons, title || 'Wybierz broń');
    }

    public buyItem(itemType: ItemId, price: number) {
        if (this.gold < price) {
            return this.showNotification('{player} nie posiada wystarczającej ilości Sz. Z.');
        }
        this.spendGold(price);
        this.addItem(itemType);
    }

    public hasItem(itemType: ItemId): boolean {
        return this.items.has(itemType);
    }

    public addItem(itemType: ItemId) {
        const item = new Item(itemType);
        this.items.put(item);
        this.showNotification('{player} zyskuje przedmiot: {name}', { name: item.name });
    }

    public removeItem(itemType: ItemId) {
        return this.items.get(itemType);
    }

    /**
     * Move player to specified location
     *
     * @param location Location to move to
     */
    public moveTo(location: Location) {
        this.location = location;
        this.showNotification('{player} przenosi się do lokalizacji {location}', {
            location: location.name,
        });
        this.emit('move', location.id);
    }

    /**
     * Move player by specified number of fields in the current player direction
     *
     * @param nrOfFields
     */
    public moveBy(nrOfFields: number) {
        switch (this.moveDirection) {
            case MoveDirection.CLOCKWISE:
                this.moveForward(nrOfFields);
                break;
            case MoveDirection.ANTICLOCKWISE:
                this.moveBack(nrOfFields);
                break;
            default:
                break;
        }
    }

    /**
     * Move player clockwise in the current region
     *
     * @param nrOfFields Number of fields to move
     */
    protected moveForward(nrOfFields: number) {
        let location = this.location;
        for (let i = 0; i < nrOfFields; i++) {
            location = location.nextLocation;
        }
        this.moveTo(location);
    }

    /**
     * Move player anticlockwise in the current region
     *
     * @param nrOfFields Number fo fields to move
     */
    protected moveBack(nrOfFields: number) {
        let location = this.location;
        for (let i = 0; i < nrOfFields; i++) {
            location = location.nextLocation;
        }
        this.moveTo(location);
    }

    public getOtherPlayersFromLocation() {
        return this.gameplay.getPlayersByLocation(this.location).filter((player) => (
            player !== this
        ));
    }

    public gainRound() {
        this.showNotification('{player} zyskuje dodatkowy ruch');
        this.rounds++;
    }

    public forfeitRound() {
        this.showNotification('{player} traci jedną turę');
        this.rounds--;
        this.emit('round-loss');
    }

    public changeNature(nature: Nature) {
        this.showNotification('{player} zmienia naturę na {nature}', { nature });
        this.nature = nature;
    }

    public becomeGood() {
        this.showNotification('{player} zmienia naturę na Dobrą');
        this.changeNature(Nature.GOOD);
    }

    public becomeEvil() {
        this.showNotification('{player} zmienia naturę na Złą');
        this.changeNature(Nature.EVIL);
    }

    public becomeChaotic() {
        this.showNotification('{player} zmienia naturę na Chaotyczną');
        this.changeNature(Nature.EVIL);
    }

    public turnIntoStone() {
        this.showNotification('{player} został zaklęty w kamień');
        this.petrifiedRounds = 3;
        // TODO: implement
    }

    public get isGood(): boolean {
        return this.nature === Nature.GOOD;
    }

    public get isChaotic(): boolean {
        return this.nature === Nature.CHAOTIC;
    }

    public get isEvil(): boolean {
        return this.nature === Nature.EVIL;
    }

    public get region(): Region {
        return this.location.region;
    }

    public get isInBottomRegion(): boolean {
        return this.location.isInBottomRegion;
    }

    public get isInMiddleRegion(): boolean {
        return this.location.isInMiddleRegion;
    }

    public get isInUpperRegion(): boolean {
        return this.location.isInUpperRegion;
    }

    public get isOnTheBridge(): boolean {
        return this.location.isOnTheBridge;
    }

    public logInfo(message: string, data: Record<string, any> = {}) {
        Log.info(this.interpolateMessage(message, data));
    }

    public async showInfo(
        info: string,
        data: Record<string, any> = {},
        options: { title?: string, confirmationText?: string, noLog?: boolean } = {},
    ) {
        if (!options.noLog) {
            this.logInfo(`[{player}] ${info}`, data);
        }
        await this.gameplay.showInfo(this.interpolateMessage(info, data), options.title, options.confirmationText);
    }

    public async showNotification(
        message: string,
        data: Record<string, any> = {},
        options: { title?: string, confirmationText?: string, noLog?: boolean } = {},
    ) {
        if (!options.noLog) {
            this.logInfo(`[{player}] ${message}`, data);
        }
        await this.gameplay.showNotification(this.interpolateMessage(message, data));
    }

    public logError(message: string, data: Record<string, any> = {}) {
        Log.error(this.interpolateMessage(message, data));
    }

    protected interpolateMessage(message: string, data: Record<string, any> = {}) {
        return interpolate(message, {
            player: this.name,
            ...data,
        });
    }

    public toString(): string {
        return this.name;
    }
}
