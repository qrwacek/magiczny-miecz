import { GameStage, GameMove } from "./game.const";

export const StageMoves: Record<GameStage, GameMove[]> = {
    start: [
        GameMove.MOVE_TO,
    ],
    meetOrExplore: [
        GameMove.EXPLORE,
        GameMove.PVP_FIGHT,
        GameMove.PVP_SKILL,
        GameMove.PVP_SPELL,
    ],
    end: [
        GameMove.END,
    ],
};
