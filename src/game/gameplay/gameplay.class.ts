import { EventEmitter } from 'eventemitter3';

import { Player } from '../player';
import { Location, LocationId, Region } from '../location';
import { ALL_ITEMS, Item, createItem } from '../item';
import { ALL_SPELLS, Spell, createSpell } from '../spell';
import { ALL_EVENTS, Event, createEvent, sortEvents } from '../event';
import { Card, CardStack, ItemSet } from '@app/card';

import { Locations } from './locations';
import { IGameContext, PlayerId } from '../game.const';
import { rollTheDice, showInfo, showNotification } from 'game/utils';

/**
 * Gameplay
 *
 * @emits state-change Emitted when state is changed. Provides current game state
 * @emits player-change Emitted on any new round. Provides current player data
 * @emits player-move Emitted when player moved to a location. Provides player info and new player location
 * @emits player-life-loss Emitted when player lost a life. Provides player info
 * @emits player-round-loss Emitted when player lost a round. Provides player info
 * @emits player-round-end Emitted when player round has ended. Provides player info
 */
export class Gameplay extends EventEmitter {
    public locations: Locations;
    public players: Record<PlayerId, Player>;

    public items: ItemSet;
    public spells: CardStack<Spell>;
    public events: CardStack<Event>;

    public constructor(
        protected context: IGameContext,
    ) {
        super();

        Object.entries(this.context.G.players!).forEach(([playerId, player]) => {
            this.players[playerId] = new Player(player, this)
        });
    }

    protected get currentPlayer() {
        return this.players[this.context.ctx.currentPlayerIndex];
    }

    public async choosePlayer(players: Player[]) {
        const selectedPlayer = await this.chooseOption(
            players.map((player) => {
                return player.name;
            }),
            'Wybierz gracza',
        );
        return players[selectedPlayer - 1];
    }

    public getLocationById(id: LocationId): Location {
        return this.locations.getById(id);
    }

    public async rollTheDice(nrOfDice: number = 1): Promise<number> {
        return await rollTheDice(nrOfDice);
    }

    public async chooseValue(label: string) {
        return chooseValue(label);
    }

    public async chooseOption(availableOptions: string[], message: string = ''): Promise<number> {
        return chooseOption(availableOptions, message);
    }

    public async yesOrNo(question: string = '') {
        const option = await this.chooseOption(['TAK', 'NIE'], question);
        return option === 1;
    }

    public async selectCard<T extends Card>(
        cards: T[],
        message: string,
        dismissible: boolean = false,
    ): Promise<T | null> {
        const options = cards.map((card) => card.name);
        if (dismissible) {
            options.push('Anuluj');
        }
        const selectedOption = await this.chooseOption(options, message);
        if (dismissible && selectedOption === options.length) {
            return null;
        }
        return cards[selectedOption - 1];
    }

    public async showInfo(info: string, title: string = 'Info', confirmationText: string = 'OK') {
        return showInfo(info, title, confirmationText);
    }

    public async showNotification(message: string, duration?: number) {
        return showNotification(message, duration);
    }

    public getPlayersByLocation(location: LocationId) {
        return Object.values(this.players).filter((player) => player.location === location);
    }

    public getPlayersByRegion(region: Region) {
        return Object.values(this.players).filter((player) => player.location.region === region);
    }

    public drawEventCards(nrOfCards: number) {
        return sortEvents(this.events.draw(nrOfCards));
    }
}
