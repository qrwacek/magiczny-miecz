import { ICardConfig } from '../card';
import { SpellId } from './spell.const';

export type ISpellConfig = ICardConfig & {
    selfOnly?: boolean;
};

export const SPELLS_CONFIG: Record<SpellId, ISpellConfig> = {
    [SpellId.FATUM]: {
        name: 'Fatum',
    },
    [SpellId.FORMULA_CZASU]: {
        name: 'Formuła Czasu',
    },
    [SpellId.FORMULA_PRZESTRZENI]: {
        name: 'Formuła Przestrzeni',
    },
    [SpellId.GOLUM]: {
        name: 'Golum',
    },
    [SpellId.HOMUNCULUS]: {
        name: 'Homunculus',
    },
    [SpellId.KAMIEN_FILOZOFICZNY]: {
        name: 'Kamień Filozoficzny',
    },
    [SpellId.KRAG_PLOMIENI]: {
        name: 'Krąg Płomieni',
    },
    [SpellId.MAGIA_I_MIECZ]: {
        name: 'Magia i Miecz',
    },
    [SpellId.MAGICZNA_WEDROWKA]: {
        name: 'Magiczna Wędrówka',
    },
    [SpellId.OCALONY]: {
        name: 'Ocalony',
    },
    [SpellId.ODMIANA_LOSU]: {
        name: 'Odmiana Losu',
    },
    [SpellId.ODRODZENIE]: {
        name: 'Odrodzenie',
    },
    [SpellId.OLSNIENIE]: {
        name: 'Olśnienie',
    },
    [SpellId.PAN_BOGACTWA]: {
        name: 'Pan Bogactwa',
    },
    [SpellId.PAN_PRZYJACIOL]: {
        name: 'Pan Przyjaciół',
    },
    [SpellId.PAN_TRZESAWISK]: {
        name: 'Pan Trzęsawisk',
    },
    [SpellId.POWIEW_SMIERCI]: {
        name: 'Powiew Śmierci',
    },
    [SpellId.SIEDEM_WICHROW]: {
        name: 'Siedem Wichrów',
    },
    [SpellId.SIEWCA_SPUSTOSZENIA]: {
        name: 'Siewca Spustoszenia',
    },
    [SpellId.SZALENSTWO]: {
        name: 'Szaleństwo',
    },
    [SpellId.WLADCA_CZAROW]: {
        name: 'Władca Czarów',
    },
    [SpellId.WLADCA_GROMU]: {
        name: 'Władca Gromu',
    },
    [SpellId.WLADCA_LODU]: {
        name: 'Władca Lodu',
    },
    [SpellId.WLADCA_ZAKLEC]: {
        name: 'Władca Zaklęć',
    },
    [SpellId.WLADCA_ZDARZEN]: {
        name: 'Władca Zdarzeń',
    },
    [SpellId.WOJNA_ZYWIOLOW]: {
        name: 'Wojna Żywiołów',
    },
    [SpellId.ZWIERCIADLO]: {
        name: 'Zwierciadło',
    },
};

export const SPELL_NUMBER: Partial<Record<SpellId, number>> ={
    [SpellId.MAGIA_I_MIECZ]: 2,
    [SpellId.SIEDEM_WICHROW]: 2,
    [SpellId.WLADCA_ZAKLEC]: 2,
}
