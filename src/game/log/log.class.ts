enum LogLevel {
    LOG = 'LOG',
    INFO = 'INFO',
    DEBUG = 'DEBUG',
    ERROR = 'ERROR',
}

export class Log {
    public static debug(message: string) {
        Log.message(message, LogLevel.DEBUG);
    }

    public static info(message: string) {
        Log.message(message, LogLevel.INFO);
    }

    public static error(message: string) {
        Log.message(message, LogLevel.ERROR);
    }

    protected static message(message: string, type: LogLevel = LogLevel.LOG) {
        const currentTime = Log.getTime();
        console.log(`(${currentTime}) [${type}] ${message}`);
    }

    protected static getTime(): string {
        const now = new Date();
        return now.toISOString().split('T')[0];
    }
}
