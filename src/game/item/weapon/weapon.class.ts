import { Item } from '../item.class';
import { ItemKind } from '../item.const';
import { WeaponType } from './weapon.const';

export class Weapon extends Item {
    public constructor(
        public id: WeaponType,
        public might: number,
    ) {
        super(id);
        this.kind = ItemKind.WEAPON;
    }
}
