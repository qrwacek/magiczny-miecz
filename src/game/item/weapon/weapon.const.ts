import { ItemId } from '../item.const';

export type WeaponType = Partial<ItemId>;
