import { ItemId } from './item.const';
import { Item } from './item.class';
import { Weapon } from './weapon';
import { Armor } from './armor';

export const createItem = (id: ItemId) => {
    switch (id) {
        case ItemId.MIECZ:
        case ItemId.SZTYLET:
            return new Weapon(id, 1);
        case ItemId.HELM:
            return new Armor(id, 1);
        case ItemId.TARCZA:
            return new Armor(id, 2);
        case ItemId.ZBROJA:
            return new Armor(id, 3);
        default:
            return new Item(id);
    }
};
