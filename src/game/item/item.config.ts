import { ICardConfig } from '../card';
import { ItemId } from './item.const';

export interface IItemConfig extends ICardConfig {};

export const ITEMS_CONFIG: Record<ItemId, IItemConfig> = {
    [ItemId.HELM]: {
        name: 'Hełm',
        description: 'Posiadacz Hełmu, który podczas walki utracił 1 punkt życia, ma prawo wykonać rzut kostką. ' +
            'Wynik równy 1 oznacza że Hełm go ochronił. Nie traci punktu Życia, aczkolwiek przegrywa walkę',
    },
    [ItemId.KIJ_I_SZNUR]: {
        name: 'Kij i Sznur',
        description: 'Mając kij i mocny sznur możesz bezpiecznie przejść przez bagna. Nie tracisz tam przedmiotu ' +
            'ani przyjaciela.',
    },
    [ItemId.KON]: {
        name: 'Koń',
        description: 'Koń może nieść 8 twoich Przedmiotów. Jeżeli go utracisz będziesz musiał pozostawić wszystkie ' +
            'Przedmioty, których nie możesz sam unieść. Przedmioty te pozostaną na Obszarze, na którym utraciłeś Konia.',
    },
    [ItemId.LATARNIA]: {
        name: 'Latarnia',
        description: 'Latarnia pozwoli ci odnaleźć drogę w ciemnościach Lodowego Lasu. Możesz przeprawić się ' +
            'w następnej turze po jej odnalezieniu. Z lasu wyjdziesz na Obszarze graniczącym z tym, ' +
            'z którego wszedłeś. Bez względu na to, czy użyłeś Latarni, czy też nie, odłóż tę Kartę. Olej w jej ' +
            'wnętrzu szybko się wypala.',
    },
    [ItemId.LODZ]: {
        name: 'Łódź',
        description: 'W następnej turze po odnalezieniu Łodzi możesz przeprawić się przez Trzęsawiska. Przeprawisz ' +
            'się do Obszaru graniczącego z tym, z którego wyruszyłeś. Bez względu na to, czy użyłeś Łodzi, ' +
            'czy też nie, odłóż tę Kartę. Nie możesz nieść Łodzi, zaś pozostawiona na brzegu szybko gnije.',
    },
    [ItemId.MAGICZNY_MIECZ]: {
        name: 'Magiczny Miecz',
        description: 'Musisz mieć Magicznym Miecz żeby wejść na Kamienny Most. Miecza nie można otrzymać w Krainie ' +
            'Dolnego Kręgu - jeśli któraś z Postaci wyciągnie tam Kartę tego Miecza, musi ją ponownie dołączyć ' +
            'do stosu Kart.',
    },
    [ItemId.MIECZ]: {
        name: 'Miecz',
        description: 'Miecz podczas walki dodaje właścicielowi 1 punkt Miecza.',
    },
    [ItemId.REKAWICE]: {
        name: 'Rękawice',
        description: 'Dzięki Rękawicom zaopatrzonym w pazury Orła nie stracisz 1 punktu Życia na Ruchomych Skałach.',
    },
    [ItemId.SZTYLET]: {
        name: 'Sztylet',
        description: 'Sztylet podczas walki dodaje właścicielowi 1 punkt Miecza.',
    },
    [ItemId.TARCZA]: {
        name: 'Tarcza',
        description: 'Posiadacz Tarczy, który podczas walki utracił 1 punkt życia, ma prawo wykonać rzut kostką. ' +
            'Wynik równy 1 lub 2 oznacza że zdołał zasłonić się Tarczą. Nie traci punktu Życia, ' +
            'aczkolwiek przegrywa walkę',
    },
    [ItemId.TARCZA_TOLIMANA]: {
        name: 'Tarcza Tolimana',
        description: 'Musisz mieć Tarczę Tolimana, żeby dostać się do Zamku Bestii',
    },
    [ItemId.ZBROJA]: {
        name: 'Zbroja',
        description: 'Posiadacz Zbroi, który podczas walki utracił 1 punkt życia, ma prawo wykonać rzut kostką. ' +
            'Wynik równy 1, 2 lub 3 oznacza że Zbroja go ochroniła. Nie traci punktu Życia, aczkolwiek przegrywa walkę',
    },
};
