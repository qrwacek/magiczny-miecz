import { Card } from '../card';
import { ItemKind, ItemId } from './item.const';
import { ITEMS_CONFIG, IItemConfig } from './item.config';

export class Item extends Card<ItemId, IItemConfig> {
    protected kind?: ItemKind;

    public constructor(id: ItemId) {
        const config = ITEMS_CONFIG[id];
        super(id, config);
    }

    public get isWeapon() {
        return this.kind === ItemKind.WEAPON;
    }

    public get isArmor() {
        return this.kind === ItemKind.ARMOR;
    }
}
