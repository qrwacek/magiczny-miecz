export enum ItemKind {
    WEAPON,
    ARMOR,
}

export enum ItemId {
    HELM = 'helm',
    KIJ_I_SZNUR = 'kij-i-sznur',
    KON = 'kon',
    LATARNIA = 'latarnia',
    LODZ = 'lodz',
    MAGICZNY_MIECZ = 'magiczny-miecz',
    MIECZ = 'miecz',
    REKAWICE = 'rekawice',
    SZTYLET = 'sztylet',
    TARCZA = 'tarcza',
    TARCZA_TOLIMANA = 'tarcza-tolimana',
    ZBROJA = 'zbroja',
}

export const ALL_ITEMS: ItemId[] = Object.values(ItemId);
