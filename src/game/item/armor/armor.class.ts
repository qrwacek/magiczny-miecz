import { Item } from '../item.class';
import { ItemKind, ItemId } from '../item.const';

export class Armor extends Item {
    public constructor(id: ItemId, protected defense: number) {
        super(id);
        this.kind = ItemKind.ARMOR;
    }

    public hasDefended(diceRollResult: number) {
        return this.defense <= diceRollResult;
    }
}
