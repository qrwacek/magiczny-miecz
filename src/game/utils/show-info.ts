import { store } from "store";
import { setInfo, selectInfo } from "store/game";
import { waitForState } from "store/utils";

export function showInfo(message: string) {
    store.dispatch(setInfo(message));
    return waitForState(selectInfo);
}
