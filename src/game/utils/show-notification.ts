import { store } from 'store';
import { addNotification, NotificationType } from 'store/notifications';

export function showNotification(
    message: string,
    type = NotificationType.INFO,
) {
    store.dispatch(addNotification({ message, type }));
}

export function showWarning(message: string) {
    showNotification(message, NotificationType.WARNING);
}

export function showError(message: string) {
    showNotification(message, NotificationType.ERROR);
}
