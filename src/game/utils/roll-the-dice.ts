import { store } from "store";
import { diceRoll, selectDiceRollResult } from "store/game";
import { waitForState } from "store/utils";

export function rollTheDice(nrOfDice: number) {
    store.dispatch(diceRoll(nrOfDice));
    return waitForState(selectDiceRollResult);
}
