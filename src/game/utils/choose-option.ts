import { store } from "store";
import { waitForState } from "store/utils";
import { setOptions, selectSelectedOption } from "store/game";

export async function chooseOption(options: string[], title = '', cta = '') {
    store.dispatch(setOptions(options, { title, cta }));
    const selectedOption = await waitForState(selectSelectedOption);
    if (selectedOption === null) {
        return null;
    }
    return options[selectedOption] || null;
}
