import { Context, GameMoveHandler } from "boardgame.io/core";
import { Plugin } from "boardgame.io/plugins";
import { LocationId } from "./location";
import { CharacterId, Nature } from "./character";
import { ItemId } from "./item";
import { EventId } from "./event";
import { SpellId } from "./spell";

export type PlayerId = string;

export enum GamePhase {
    CHARACTER_SELECT = 'characterSelect',
    PLAY = 'play',
}

export enum GameStage {
    START = 'start',
    MEET_OR_EXPLORE = 'meetOrExplore',
    END = 'end',
}

export enum GameMove {
    CHOOSE_CHARACTER = 'chooseCharacter',
    DRAW_SPELL = 'drawSpell',
    MOVE_TO = 'moveTo',
    PVP_FIGHT = 'pvpFight',
    PVP_SPELL = 'pvpSpell',
    PVP_SKILL = 'pvpSkill',
    EXPLORE = 'explore',
    END = 'end',
}

export type Moves = {
    chooseCharacter: (character: CharacterId) => void;
    drawSpell: () => void;
    moveTo: (location: LocationId) => void;
    pvpFight: () => void;
    pvpSpell: () => void;
    pvpSkill: () => void;
    explore: () => void;
    end: () => void;
}

export type MoveHandler<K extends GameMove> = GameMoveHandler<
    IGameState,
    PlayerId,
    GamePhase,
    Moves[K]
>;

export interface IPlayerMove<T extends string = string> {
    id: T;
    name: string;
    stage: GameStage;
}

export interface IPlayerState {
    name: string;
    character?: CharacterId;
    nature?: Nature;
    might?: number;
    magic?: number;
    gold?: number;
    life?: number;
    items?: ItemId[];
    spells?: SpellId[];
    events?: EventId[];
    location?: LocationId;
    enemiesMight?: number;
    enemiesMagic?: number;
    isDead?: boolean;
}

export interface ILocationState {
    id: LocationId;
    index: number;
    events: EventId[];
    items: ItemId[];
    spells: SpellId[];
    gold: number;
}

export interface IGameState {
    start?: string;
    players?: Record<PlayerId, IPlayerState>;
    locations?: ILocationState[];
    events?: EventId[];
    items?: ItemId[];
    spells?: SpellId[];
    diceRoll?: number;
}

export interface IGameContext {
    G: IGameState,
    ctx: Context<PlayerId, GamePhase, GameStage>,
    moves: Moves,
}

export type GamePlugin = Plugin<IGameState, Moves, PlayerId, GamePhase>;
