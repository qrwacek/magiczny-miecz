import { CHARACTER_CONFIG } from 'game/character';
import { drawSpell } from './draw-spell';
import { MoveHandler, GameMove, IPlayerState } from '../game.const';

export const chooseCharacter: MoveHandler<GameMove.CHOOSE_CHARACTER> = (
    G,
    ctx,
    character,
) => {
    const characterConfig = CHARACTER_CONFIG[character];

    const player: IPlayerState = { ...G.players![ctx.currentPlayer] };

    player.character = character;
    player.nature = characterConfig.nature;
    player.location = characterConfig.location;

    player.might = 0;
    player.magic = 0;
    player.gold = characterConfig.gold || 0;
    player.life = 3;

    player.items = characterConfig.items || [];
    player.spells = [];
    player.events = [];

    player.enemiesMight = 0;
    player.enemiesMagic = 0;

    G.players![ctx.currentPlayer] = player;

    if (characterConfig.spells) {
        for (let i = 0; i < characterConfig.spells; i++) {
            drawSpell(G, ctx);
        }
    }

    ctx.events.endTurn();
};
