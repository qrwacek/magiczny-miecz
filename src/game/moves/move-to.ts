import { GameMove, MoveHandler } from "../game.const";

export const moveTo: MoveHandler<GameMove.MOVE_TO> = (G, ctx, location) => {
    console.log('MOVE PLAYER', ctx.currentPlayer, 'TO', location);
    G.players![ctx.currentPlayer].location = location;
    ctx.events.endStage();
}
