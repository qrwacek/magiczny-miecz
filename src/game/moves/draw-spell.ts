import { MoveHandler, GameMove, IPlayerState } from '../game.const';

export const drawSpell: MoveHandler<GameMove.DRAW_SPELL> = (G, ctx) => {
    const spells = [ ...G.spells! ];

    if (!spells || spells.length === 0) {
        return;
    }

    const spell = spells.pop()!;

    const player: IPlayerState = { ...G.players![ctx.currentPlayer] };
    player.spells!.push(spell);

    G.players![ctx.currentPlayer] = player;
    G.spells = spells;
}
