import { LocationId } from './location.const';
import { ICardConfig } from 'game/card';

export type LocationConfig = ICardConfig & {};

export const LOCATIONS_CONFIG: Partial<Record<LocationId, LocationConfig>> = {
    [LocationId.OSADA]: {
        name: 'Osada',
        description:
            'Możesz tu odwiedzić:\n\n' +
            'Czarownicę - rzuć kostką:\n' +
            '1, 2 - tracisz 1 punkt Miecza\n' +
            '3, 4 - zyskujesz 1 punkt Magii lub 1 punkt Miecza\n' +
            '5 - zyskujesz 1 Zaklęcie\n' +
            '6 - zostajesz zignorowany\n\n' +
            'Płatnerza - możesz u niego kupić:\n' +
            'Miecz - 2 Sz. Z.\n' +
            'Sztylet - 3 Sz. Z.\n' +
            'Hełm - 1 Sz. Z.\n\n' +
            'Medyka - za każdą Sztukę Złota przywróci ci 1 punkt Życia',
    },
    [LocationId.STEP]: {
        name: 'Step',
    },
    [LocationId.MOKRADLA]: {
        name: 'Mokradła',
    },
    [LocationId.CZARCI_MLYN]: {
        name: 'Czarci młyn',
        description:
            'Jeżeli jesteś dobry, tracisz jedno życie.\n\n' +
            'Chaotyczny - rzuć kostką:\n' +
            '1, 2, 3 - zyskujesz 1 Życie\n' +
            '4, 5, 6 - tracisz 1 życie\n\n' +
            'Zły - możesz wezwać siły ciemności:\n' +
            '1 - zyskujesz 1 punkt Miecza\n' +
            '2 - zyskujesz 1 punkt Magii\n' +
            '3 - otrzymujesz 1 Zaklęcie\n' +
            '4 - zyskujesz dodatkowy ruch\n' +
            '5 - tracisz 1 turę\n' +
            '6 - tracisz 1 Życie\n'
    },
    [LocationId.KRAG_MOCY]: {
        name: 'Krąg Mocy',
        description:
            'Musisz rzucić kostką:\n\n' +
            '1 - zostajesz zaatakowany przez Strażnika Kręgu (Miecz 5)\n' +
            '2, 3 - tracisz jedną turę\n' +
            '4, 5 - nic się nie dzieje\n' +
            '6 - zuskujesz 1 punkt Magii',
    },
    [LocationId.STUDNIA_WIECZNOSCI]: {
        name: 'Studnia Wieczności',
        description:
            'Jeżeli jesteś Dobry, możesz odzyskać punkty Życia z początku gry',
    },
    [LocationId.BEZDROZA]: {
        name: 'Bezdroża',
    },
    [LocationId.GROD]: {
        name: 'Gród',
        description:
            'Możesz tu odwiedzić\n\n' +
            'Wróżbitę - rzuć kostką:\n' +
            '1 - zyskujjesz 1 Zaklęcie\n' +
            '2 - zostajesz Zaklęty w Kamień\n' +
            '3 - jeżeli jesteś Zły stajesz się Dobry, jeżeli jesteś Chaotyczny stajesz się Zły\n' +
            '4-6 - zostajesz zignorowany\n\n' +
            'Lichwiarza - możesz wymienić dowolne Przedmioty na złoto (1 Sz. Z. za każdy)',
    },
    [LocationId.MROZNE_PUSTKOWIE]: {
        name: 'Mroźne Pustkowie',
    },
    [LocationId.KARCZMA]: {
        name: 'Karczma',
        description:
            'Musisz rzucić kostką:\n\n' +
            '1 - przegrywasz w kości 1 Sz. Z.\n' +
            '2 - wygrywasz 1 Sz. Z.\n' +
            '3 - musisz tu nocować, tracisz 1 turę\n' +
            '4 - musisz stawić czoła miejscowemu osiłkowi (Miecz 4)\n' +
            '5 - poczęstowano cię eliksirem, dzięki któremu możesz przenieść się do dowolnego miejsca w tym Kręgu\n' +
            '6 - eliksir, który ci podano, może przenieść cię do Świątyni Nemed',
    },
    [LocationId.UROCZYSKO]: {
        name: 'Uroczysko',
    },
    [LocationId.KURHAN]: {
        name: 'Kurhan',
        description:
            'Musisz rzucić kostką:\n\n' +
            '1 - zyskujesz 1 punkt Miecza\n' +
            '2, 3 - nic się nie dzieje\n' +
            '4, 5 - zostajesz opętany przez duchy, tracisz 1 turę\n' +
            '6 - zostajesz zaatakowany przez Ducha (Magia 4)',
    },
};
