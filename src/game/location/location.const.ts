export enum Region {
    BOTTOM,
    MIDDLE,
    UPPER,
    BRIDGE,
}

export enum LocationId {
    // BOTTOM REGION
    OSADA = 'osada',
    STEP = 'step',
    MOKRADLA = 'mokradla',
    CZARCI_MLYN = 'czarci-mlyn',
    KRAG_MOCY = 'krag-mocy',
    STUDNIA_WIECZNOSCI = 'studnia-wiecznosci',
    BEZDROZA = 'bezdroza',
    GROD = 'grod',
    MROZNE_PUSTKOWIE = 'mrozne-pustkowie',
    KARCZMA = 'karczma',
    UROCZYSKO = 'uroczysko',
    KURHAN = 'kurhan',

    // MIDDLE REGION
    TWIERDZA_STRZEGACA_DROG = 'twierdza-strzegaca-drog',
    PRZELECZ_WICHROW = 'przelecz-wichrow',
    PRZEPRAWA = 'przeprawa',
    DOLINA_CIENIA = 'dolina-cienia',
    WRZOSOWISKA = 'wrzosowiska',
    WIEZA_PRZEZNACZENIA = 'wieza-przeznaczenia',
    STRAZNIK_MAGICZNYCH_WROT = 'straznik-magicznych-wrot',
    MAGICZNE_WROTA = 'magiczne-wrota',
    PLASKOWYZ_MGIEL = 'plaskowyz-mgiel',
    SWIATYNIA_BOGINI_NEMED = 'swiatynia-bogini-nemed',
    ZACZAROWANE_WZGORZA = 'zaczarowane-wzgorza',
    LAS_BLEDNYCH_OGNI = 'las-blednych-ogni',
    PUSTELNIA = 'pustelnia',
    ROWNINA_SAMOTNYCH_SKAL = 'rownina-samotnych-skal',
    MROCZNA_POLANA = 'mroczna-polana',

    // UPPER REGION
    URWISKO = 'urwisko',
    RUINY_TWIERDZY = 'ruiny-twierdzy',
    SWIATYNIA_TOLIMANA = 'swiatynia-tolimana',
    DOLINA_CZASZEK = 'dolina-czaszek',
    BAGNA = 'bagna',
    RUCHOME_SKALY = 'ruchome-skaly',
    ROWNINA_TRAW = 'rownina-traw',
    ROZSTAJNE_DROGI = 'rozstajne-drogi',
    ZAMEK = 'zamek',
    WYMARLE_MIASTO = 'wymarle-miasto',
    KRYPTA_UPIOROW = 'krypta-upiorow',
    ROWNINA_SNU = 'rownina-snu',
    KAMIENNY_LAS = 'kamienny-las',
    WILCZY_PAROW = 'wilczy-parow',

    // BRIDGE
    WEJSCIE_NA_MOST = 'wejscie-na-most',
    PULAPKA = 'pulapka',
    GRA_ZE_SMIERCIA = 'gra-ze-smiercia',
    DEMON_ZAGLADY = 'demon-zaglady',
    ZAMEK_BESTII = 'zamek-bestii',
    MONSTRUM = 'monstrum',
    CERBER = 'cerber',
    MAGICZNA_PULAPKA = 'cerber',
}

export const LOCATIONS_BY_REGION: Record<Region, LocationId[]> = {
    [Region.BOTTOM]: [
        LocationId.OSADA, LocationId.STEP, LocationId.MOKRADLA, LocationId.CZARCI_MLYN, LocationId.KRAG_MOCY,
        LocationId.STUDNIA_WIECZNOSCI, LocationId.BEZDROZA, LocationId.GROD, LocationId.MROZNE_PUSTKOWIE,
        LocationId.KARCZMA, LocationId.UROCZYSKO, LocationId.STEP, LocationId.MOKRADLA, LocationId.KURHAN,
    ],
    [Region.MIDDLE]: [
        LocationId.TWIERDZA_STRZEGACA_DROG, LocationId.PRZELECZ_WICHROW, LocationId.PRZEPRAWA,
        LocationId.DOLINA_CIENIA, LocationId.WRZOSOWISKA, LocationId.WIEZA_PRZEZNACZENIA,
        LocationId.STRAZNIK_MAGICZNYCH_WROT, LocationId.MAGICZNE_WROTA, LocationId.PLASKOWYZ_MGIEL,
        LocationId.SWIATYNIA_BOGINI_NEMED, LocationId.ZACZAROWANE_WZGORZA, LocationId.LAS_BLEDNYCH_OGNI,
        LocationId.PUSTELNIA, LocationId.ROWNINA_SAMOTNYCH_SKAL, LocationId.PRZEPRAWA,
        LocationId.MROCZNA_POLANA,
    ],
    [Region.UPPER]: [
        LocationId.URWISKO, LocationId.RUINY_TWIERDZY, LocationId.SWIATYNIA_TOLIMANA, LocationId.DOLINA_CZASZEK,
        LocationId.BAGNA, LocationId.RUCHOME_SKALY, LocationId.URWISKO, LocationId.ROWNINA_TRAW,
        LocationId.ROZSTAJNE_DROGI, LocationId.ZAMEK, LocationId.WYMARLE_MIASTO, LocationId.RUCHOME_SKALY,
        LocationId.BAGNA, LocationId.KRYPTA_UPIOROW, LocationId.ROWNINA_SNU, LocationId.ROZSTAJNE_DROGI,
        LocationId.KAMIENNY_LAS, LocationId.WILCZY_PAROW,
    ],
    [Region.BRIDGE]: [
        LocationId.WEJSCIE_NA_MOST, LocationId.PULAPKA, LocationId.GRA_ZE_SMIERCIA, LocationId.DEMON_ZAGLADY,
        LocationId.ZAMEK_BESTII, LocationId.MONSTRUM, LocationId.CERBER, LocationId.MAGICZNA_PULAPKA,
        LocationId.WEJSCIE_NA_MOST,
    ],
};

export interface ILocationDimension {
    x: number;
    y: number;
    width: number;
    height: number;
}

export const LOCATIONS_DIMENSIONS: Partial<
    Record<LocationId, ILocationDimension | ILocationDimension[]>
> = {

    /*** BOTTOM REGION ***/

    [LocationId.OSADA]: {
        x: 712,
        y: 684,
        width: 304,
        height: 420,
    },
    [LocationId.STEP]: [
        {
            x: 1254,
            y: 684,
            width: 320,
            height: 138,
        },
        {
            x: 708,
            y: 1688,
            width: 338,
            height: 294,
        },
    ],
    [LocationId.MOKRADLA]: [
        {
            x: 1634,
            y: 764,
            width: 142,
            height: 228,
        },
        {
            x: 712,
            y: 1388,
            width: 340,
            height: 290,
        },
    ],
    [LocationId.CZARCI_MLYN]: {
        x: 1420,
        y: 1020,
        width: 344,
        height: 298,
    },
    [LocationId.KRAG_MOCY]: {
        x: 1424,
        y: 1318,
        width: 344,
        height: 320,
    },
    [LocationId.STUDNIA_WIECZNOSCI]: {
        x: 1422,
        y: 1634,
        width: 342,
        height: 348,
    },
    [LocationId.BEZDROZA]: {
        x: 1428,
        y: 1984,
        width: 328,
        height: 304,
    },
    [LocationId.GROD]: {
        x: 1568,
        y: 2288,
        width: 190,
        height: 438,
    },
    [LocationId.MROZNE_PUSTKOWIE]: {
        x: 1258,
        y: 2422,
        width: 214,
        height: 308,
    },
    [LocationId.KARCZMA]: {
        x: 698,
        y: 2412,
        width: 288,
        height: 308,
    },
    [LocationId.UROCZYSKO]: {
        x: 704,
        y: 1978,
        width: 342,
        height: 434,
    },
    [LocationId.KURHAN]: {
        x: 714,
        y: 1104,
        width: 338,
        height: 292,
    },

    /*** MIDDLE REGION ***/

    /*** UPPER REGION ***/
};

