import { EnemyPower } from './enemy.const';

export class Enemy {
    public constructor(
        public readonly name: string,
        public readonly powerType: EnemyPower,
        public readonly powerPoints: number,
        protected options: {
            isImmuneToSpells?: boolean;
            isImmuneToWeapons?: boolean;
            isImmuneToItems?: boolean;
            isImmuneToMagicItems?: boolean;
        } = {},
    ) {

    }

    public get info(): string {
        const powerType = this.usesMagic ? 'Magia' : 'Miecz';
        return `${this.name} (${powerType} ${this.powerPoints})`;
    }

    public get usesMight(): boolean {
        return this.powerType === EnemyPower.MIGHT;
    }

    public get usesMagic(): boolean {
        return this.powerType === EnemyPower.MAGIC;
    }

    public get isImmuneToSpells() {
        return this.options.isImmuneToSpells;
    }

    public get isImmuneToWeapons() {
        return this.options.isImmuneToWeapons;
    }

    public get isImmuneToItems() {
        return this.options.isImmuneToItems;
    }

    public get isImmuneToMagicItems() {
        return this.options.isImmuneToMagicItems;
    }
}

export class EnemyMight extends Enemy {
    public constructor(name: string, mightPoints: number) {
        super(name, EnemyPower.MIGHT, mightPoints);
    }

    get might() {
        return this.powerPoints;
    }
}

export class EnemyMagic extends Enemy {
    public constructor(name: string, magicPoints: number) {
        super(name, EnemyPower.MAGIC, magicPoints);
    }

    get magic() {
        return this.powerPoints;
    }
}
