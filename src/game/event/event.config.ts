import { ICardConfig } from '../card';
import { EventId } from './event.const';

export type IEventConfig = ICardConfig & {
    order?: number;
    isPermanent?: boolean;
};

export const EVENTS_CONFIG: Record<EventId, IEventConfig> = {
    [EventId.BURZA_SIEDMIU_SLONC]: {
        name: 'Burza Siedmiu Słońc',
        description: 'Nad Krąg w którym jesteś dotarły wiry czasu spowodowane przez gwałtownę Burzę Słoneczną. ' +
            'Wszystkie Postacie tracą 1 turę. Potem nagły wybuch aktywności Słońc ustaje, należy odłożyć Kartę.',
    },
    [EventId.DANINA]: {
        name: 'Danina',
        description:
            'Bestia postanowiła nałożyć daninę w wysokości 1 Sz. Z. Rzuć kostką, by stwierdzić kto złoży opłatę:\n\n' +
            '1 - Dobrzy\n' +
            '2 - Chaotyczni\n' +
            '3 - Źli\n' +
            '4 - Wędrujący po Dolnym Kręgu\n' +
            '5 - Wędrujący po Środkowym Kręgu\n' +
            '6 - Wędrujący po Górnym Kręgu',
    },
    [EventId.GODZINA_DUCHOW]: {
        name: 'Godzina Duchów',
        description:
            'Nadeszła godzina Duchów. Może je wezwać każda Zła Postać. Rzuć kostką by określić skutek wezwania:\n\n' +
            '1, 2 - Zyskuje 1 Zaklęcie\n' +
            '3, 4 - Odzyskuje 1 punkt Życia\n' +
            '5, 6 - Traci 1 turę',
    },
    [EventId.KOMETA]: {
        name: 'Kometa',
        description:
            'Na Krainę, po której wędrujesz spada apokaliptyczna Gwiazda. W katastrofie giną wszyscy Nieznajomi.',
    },
    [EventId.MAGICZNA_TABLICA]: {
        name: 'Magiczna Tablica',
        description: 'Znalazłeś kamienną tablicę, na której wyryte zostały magiczne formuły z zamierzchłych czasów. ' +
            'Natychmiast uzyskujesz taką liczbę Zaklęć, na jaką pozwala ci twoja Magia. Zaklęcia nabiorą mocy, ' +
            'gdy roztrzaskasz Tablicę - odłóż jej kartę.',
    },
    [EventId.MGLA]: {
        name: 'Mgła',
        description: 'Z Płaskowyżu zeszła gęsta Mgła, okrywając nieprzeniknioną bielą wszystkie Krainy. Przez 2 tury ' +
            'Postacie mogą przebywać tylko 2 Obszary (1 Obszar na turę). Potem Mgła rozpływa się - odłóż jej Kartę.',
    },
    [EventId.POLUDNICA]: {
        name: 'Południca',
        description: 'Od pewnego czasu towarzyszy ci Południca. Musisz ją zabrać jako Przyjaciela chociaż jej ' +
            'obecność osłabia cię - możesz poruszać się tylko o 1 Obszar na turę. Jedynym sposobem pozbycia się ' +
            'Południcy jest przeprawa przez Trzęsawiska lub Lodowy Las. Gdy to zrobisz, odłóż jej Kartę.',
    },
    [EventId.POSLANCY_BOGOW]: {
        name: 'Posłańcy Bogów',
        description: 'Spotkałeś potężne Dobre Duchy, wędrujące po tej Krainie. Jeżeli ty także jesteś Dobry, Duchy ' +
            'obdarują cię 1 punktem Życia, jeżeli jesteś Zły, tracisz 1 Życie. Posłańcy ignorują Chaotyczne Postacie.',
    },
    [EventId.PRZESILENIE]: {
        name: 'Przesilenie',
        description: 'Potężny Żywioł Magii rozpętał nad Światem gwałtowną burzę. Wszystkie Zaklęcia utraciły swoją ' +
            'moc. Burza kończy się tak nagle jak się rozpoczęła. Odłóż tę Kartę i wszystkie Karty Zaklęć, znajdujące ' +
            'się w posiadaniu Postaci (we wszystkich Kręgach).',
    },
    [EventId.SABAT_CZAROWNIC]: {
        name: 'Sabat Czarownic',
        description: 'Natknąłeś się na Czarownice tańczące w księżycowej poświacie. Jeżeli jesteś Zły otrzymujesz ' +
            '1 punkt Magii, jeżeli jesteś Dobry lub Chaotyczny zamieniasz się w Złego. Potem Czarownice kończą swój ' +
            'taniec - odłóż Kartę.',
    },
    [EventId.SLUP_OGNIA]: {
        name: 'Słup Ognia',
        description: 'Stanąłeś przed ogromnym Słupem Ognia, od którego emanuje potężna siła Dobra. Jeżeli jesteś ' +
            'Dobry otrzymujesz 1 punkt Magii, jeżeli jesteś Zły lub Chaotyczny, zamieniasz się w Dobrego. Potem ' +
            'Słup Ognia znika - odłóż tę Kartę.',
    },
    [EventId.STRAZ]: {
        name: 'Straż',
        description: 'Niespodziewanie spotykasz wędrujący po okolicy oddział straży z Zamku Bestii. Strażnicy ' +
            'natychmiast zawracają cię na Obszar, z którego rozpocząłeś wędrówkę. Potem ruszają w dalszą drogę - ' +
            'odłóż Kartę.',
    },
    [EventId.TURNIEJ_RYCERSKI]: {
        name: 'Turniej Rycerski',
        description: 'Możesz wyzwać na pojedynek każdą Postać, jeśli tylko nie znajduje się na Moście. Zostaje ona ' +
            'natychmiast przeniesiona do twojego Obszaru. Walka odbywa się według zwykłych zasad. Po jej zakończeniu ' +
            'odłóż tę Kartę.',
    },
    [EventId.UKLAD_PLANET]: {
        name: 'Układ Planet',
        description: 'Przy tym szczególnym układzie planet, na czas 1 tury podwojona zostaje Magia wszystkich ' +
            'Demonów. Po zakończeniu tury należy odłożyć Kartę.',
    },
    [EventId.ZACMIENIE_SLONC]: {
        name: 'Zaćmienie Słońc',
        description: 'Zaćmienie Słońc stworzyło Siłom Ciemności drogę do Świata. Dobre i Chaotyczne Postacie tracą ' +
            '1 turę. Potem Zaćmienie kończy się - odłóż tę Kartę.',
    },
    [EventId.ZAKLETA_SCIEZKA]: {
        name: 'Zaklęta Ścieżka',
        description:
            'Wszedłeś na Zaklętą Ścieżkę, nie wiedząc, dokąd cię zaprowadzi. Rzuć kostką by się tego dowiedzieć:\n\n' +
            '1 - do Równiny Snu\n' +
            '2 - do Równiny Traw\n' +
            '3 - do Doliny Cienia\n' +
            '4 - do Mrocznej Polany\n' +
            '5 - do Osady\n' +
            '6 - do Karczmy\n\n' +
            'Po zejściu ze ścieżki odłóż Kartę.',
    },
    [EventId.ZARAZA]: {
        name: 'Zaraza',
        description: 'W Kręgu, w którym jesteś, wybuchła Zaraza. Wszystkie znajdujące się w nim Postacie włącznie z ' +
            'tobą tracą 1 Życie. Następnie Zaraza wygasa, odłóż tę Kartę.',
    },
    [EventId.ZASADZKA]: {
        name: 'Zasadzka',
        description: 'Wpadłeś w Zasadzkę żądnego łupów złoczyńcy. Tracisz całe Złoto i wszystkie Przedmioty. ' +
            'Łupieżca ukrywa swoją zdobycz na Wrzosowiskach (połóż tam Karty i żetony), potem odchodzi w inne strony ' +
            '- odłóż jego Kartę.',
    },
    [EventId.ZATRUTE_ZIOLA]: {
        name: 'Zatrute Zioła',
        description: 'Trafiłeś na łąkę Zatrutych Ziół. Jeżeli jesteś Zły, ich zapach pozwoli ci zyskać 1 Życie, ' +
            'jeśli jesteś Dobry - tracisz 1 Życie. Zapach Ziół nie działa na Chaotyczne Postacie. Natychmiast ' +
            'opuszczasz to miejsce - odłóż Kartę.',
    },
    [EventId.ZLY_DUCH]: {
        name: 'Zły Duch',
        description: 'Od pewnego czasu towarzyszy ci Zły Duch. Musisz zabrać go jako Przyjaciela. Natychmiast ' +
            'opuszczają cię wszyscy dotychczasowi Przyjaciele (z wyjątkiem Południcy). Nie możesz zdobywać nowych ' +
            'Przyjaciół, dopóki nie uwolnisz się od niego, Odwiedzając Pustelnię. Po wizycie u Pustelnika odłóż Kartę.',
    },

    [EventId.CYKLOP]: {
        name: 'Cyklop',
        description: 'Bezlitosny Cyklop obraca tę okolicę w ruinę. Pozostanie tu, aż ktoś go pokona.'
    },
    [EventId.CZARNA_HYBRYDA]: {
        name: 'Czarna Hybryda',
        description:
            'Hybryda stworzona niegdyś przez magów w walce z bogiem Tolimanem stanęła na straży tego Obszaru. ' +
            'Pozostanie tu aż ktoś ją pokona',
    },
    [EventId.CZERWONA_HYBRYDA]: {
        name: 'Czerwona Hybryda',
        description:
            'Hybryda stworzona ongiś przez czarnoksiężników walczących z boginią Nemed stoi na straży tego Obszaru. ' +
            'Pozostanie tu, aż ktoś ją pokona',
    },
    [EventId.FOMORAIG]: {
        name: 'Fomoraig',
        description:
            'Zawędrował tu okrutny rycerz o koziej głowie z rodu Fomoraigów. Pozostanie w okolicy, aż ktoś go pokona.',
    },
    [EventId.HADRON]: {
        name: 'Hadron',
        description: 'Bezlitosny Hadron narzucił swoje panowanie w tej okolicy. Będzie nią władał, aż ktoś go pokona.',
    },
    [EventId.LEWIATAN]: {
        name: 'Lewiatan',
        description:
            'Lewiatan może pojawić się na Mokradłach, przy Przeprawie lub na Bagnach, połóż jego Kartę na którymś ' +
            'z tych Obszarów nie zajętym przez inną Postać (jeśli nie ma takiego Obszaru, odłóż Kartę). ' +
            'Potwór pozostanie tam, aż ktoś go pokona.'
    },
    [EventId.LOS]: {
        name: 'Łoś',
        description: 'Zły duch zaklęty w Łosia sieje spustoszenie w okolicy. Pozostanie tu, aż ktoś go pokona.'
    },
    [EventId.NIEDZWIEDZ]: {
        name: 'Niedźwiedź',
        description:
            'Tajemnicze siły zła przyjęły postać budzącego przerażenie Niedźwiedzia, który rozszarpuje każdą ' +
            'napotkaną istotę. Niedźwiedź pozostanie na tym Obszarze, aż ktoś go pokona.'
    },
    [EventId.NOBBIN]: {
        name: 'Nobbin',
        description: 'Okrytnu Nobbin zawładnął tą okolicą. Pozostanie tu, aż ktoś go pokona.'
    },
    [EventId.PRZYBYSZ_Z_KRAINY_CIENI]: {
        name: 'Przybysz z Krainy Cieni',
        description:
            'Potworny Przybysz z Krainy Cieni będzie wyniszczał ten Obszar, dopóki ktoś go nie pokona. ' +
            'Przeciw Przybyszowi nie można używać Zaklęć, Magicznych Przedmiotów ani Broni.',
    },
    [EventId.SMOK]: {
        name: 'Smok',
        description: 'Krwawy Smok zamieszkuje tę okolicę. Pozostanie tu, aż ktoś go pokona.',
    },
    [EventId.SOBOWTOR]: {
        name: 'Sobowtór',
        description:
            'Sobowtór to monstrum, które tworzy sama Postać, ponieważ powstaje z jej lęku i nienawiści. ' +
            'Posiada zawsze tyle punktów Miecza, ile jego przeciwnik. Pozostanie tu, aż ktoś go pokona.'
    },
    [EventId.SNIEZNE_MONSTRUM]: {
        name: 'Śnieżne Monstrum',
        description:
            'Na tym Obszarze pojawił się nagle Śnieżny Potwór. Będzie napadał na Postacie, aż któraś z nich go pokona.',
    },
    [EventId.TROJGLOWY_SMOK]: {
        name: 'Trójgłowy Smok',
        description:
            'Nad całą okolicą roztoczył panowanie Trójgłowy Smok. Postać która podejmie z nim walkę, będzie musiała ' +
            'pokonać jego trzy głowy (każda głowa ma 2 punkty Miecza). Jeśli przegra, głowy, które odcięła odrastają.' +
            ' Smok pozostanie tu, aż ktoś go pokona.'
    },
    [EventId.WEDROWIEC]: {
        name: 'Wędrowiec',
        description:
            'Po okolicy wędruje potwór pod postacią Wędrowca. Rzuć kostką: 1, 2 lub 3 oznacza że nie dałeś się zwieść' +
            ' i w porę umknąłeś potworowi. Inny wynik: musisz rozpocząć walkę. Potwór pozostanie tu, aż ktoś go pokona.'
    },
    [EventId.WILK]: {
        name: 'Wilk',
        description: 'Opiorny Wilk każdej nocy poluje w tej okolicy. Pozostanie tu, aż ktoś go pokona.'
    },
    [EventId.WILKOLAK]: {
        name: 'Wilkołak',
        description: 'Potężny Wilkołak sieje spustoszenie w tej okolicy. Pozostanie tu, aż ktoś go pokona.'
    },
    [EventId.ZLOCZYNCA]: {
        name: 'Złoczyńca',
        description:
            'W tej okolicy zaczaił się żądny łupów Złoczyńca. Pozostanie tu, aż ktoś go pokona. Każdej pokonanej ' +
            'Postaci Złoczyńca zabiera do wyboru: 1 Sztukę Złota lub jeden Przedmiot.'
    },

    [EventId.DUCH_CIEMNOSCI]: {
        name: 'Duch Ciemności',
        description: 'Duch Ciemności będzie niepodzielnie władał tym Obszarem, aż ktoś go pokona.'
    },
    [EventId.DUCH_ZAGLADY]: {
        name: 'Duch Zagłady',
        description: 'Duch Zagłady będzie siał zniszczenie w tej okolicy, aż ktoś go pokona.',
    },
    [EventId.KSIAZE_DEMONOW]: {
        name: 'Książę Demonów',
        description: 'Nad okolicą zapanował niepodzielnie Książę Demonów. Pozostanie tu, aż ktoś go pokona.',
    },
    [EventId.DEMON]: {
        name: 'Demon',
        description: 'W tej okolicy pojawił się potężny Demon. Pozostanie tu, aż ktoś go pokona.',
    },
    [EventId.UPIOR]: {
        name: 'Upiór',
        description: 'Rzuć kostką, by określić gdzie pojawi się Upiór:\n\n' +
            '1 - w Osadzie\n' +
            '2 - w Grodzie\n' +
            '3 - w Dolinie Cienia\n' +
            '4 - na Mrocznej polanie\n' +
            '5 - W Krypcie Upiorów\n' +
            '6 - w Wymarłym Mieście\n\n' +
            'Upiór pozostanie tam, aż ktoś go pokona.'
    },
    [EventId.WAMPIR]: {
        name: 'Wampir',
        description: 'Wampir będzie zamieszkiwał okolicę, aż zostanie pokonany.',
    },
    [EventId.WIDMO]: {
        name: 'Widmo',
        description: 'W okolicy pojawiło się straszliwe Widmo. Pozostanie tu, aż ktoś je pokona.'
    },
    [EventId.ZJAWA]: {
        name: 'Zjawa',
        description: 'Mroczna Zjawa budzi przerażenie i popłoch wśród mieszkańców tej okolicy. Pozostanie tu, ' +
            'aż ktoś ją pokona.',
    },

    [EventId.CUDOTWORCA]: {
        name: 'Cudotwórca',
        description: 'Cudotwórca będzie mieszkał na tym Obszarze do końca rozgrywki. Każdej Postaci przywróci ' +
            '2 punkty Życia, podczas każdych odwiedzin tylko do wysokości startowej - 4 punktów.',
        isPermanent: true,
    },
    [EventId.CZARODZIEJ]: {
        name: 'Czarodziej',
        description: 'Ten Obszar do końca rozgrywki będzie siedzibą dobrego Czarodzieja. Każda Dobra Postać, ' +
            'która tu zawita, otrzyma 1 Zaklęcie, jeżeli tylko pozwala na to jej Magia.',
        isPermanent: true,
    },
    [EventId.DZIKI_RUMAK]: {
        name: 'Dziki Rumak',
        description: 'Dzięki temu spotkaniu możesz natychmiast zyskać dodatkowy ruch. Niezależnie od tego, ' +
            'czy uczynisz tak, czy nie, Rumak po chwili zniknie ci z oczu.',
    },
    [EventId.DOBRE_BOSTWO]: {
        name: 'Dobre Bóstwo',
    },
};
