export interface ICardConfig {
    name: string;
    description?: string;
    image?: string;
}
