import { EventEmitter } from 'eventemitter3';
import { getRandomNumber } from 'utils/numbers';
import { Card } from '../card.class';

/**
 * Represents unsorted card set (e. g. cards owned by player)
 *
 * @emits change Emitted when set contents changes
 */
export class CardSet<T extends Card, K extends string> extends EventEmitter {
    constructor(
        protected cards: T[] = []
    ) {
        super();
    }

    public get length(): number {
        return this.cards.length;
    }

    public get all(): T[] {
        return this.cards;
    }

    public get(id: K) {
        const cardIndex = this.cards.findIndex((card) => card.is(id));
        if (cardIndex !== -1) {
            const card = this.cards.splice(cardIndex, 1)[0];
            this.emit('change', this.cards);
            return card;
        }
    }

    public getAll() {
        const cards = this.cards.splice(0);
        this.emit('change', this.cards);
        return cards;
    }

    public getRandom() {
        if (this.cards.length === 0) {
            return null;
        }
        const randomIndex = getRandomNumber(0, this.cards.length - 1);
        const card = this.cards.splice(randomIndex, 1)[0];
        this.emit('change', this.cards);
        return card;
    }

    public find(id: K) {
        return this.cards.find((card) => card.is(id));
    }

    public has(id: K): boolean {
        return this.cards.some(((card) => card.is(id)));
    }

    public put(...cards: T[]): void {
        this.cards.push(...cards);
        this.emit('change', this.cards);
    }

    public forEach(predicate: (card: T, index: number, cards: T[]) => void): void {
        this.cards.forEach(predicate);
    }

    public map<M>(callback: (card: T, index: number, cards: T[]) => M): M[] {
        return this.cards.map(callback);
    }

    public onChange(callback: (cards: T[]) => void): void {
        this.on('change', callback);
    }
}
