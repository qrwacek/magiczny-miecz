import { Card } from './card.class';

describe('Card', () => {
    it('should create a card from config', () => {
        const card1 = new Card('card1', { name: 'Card 1' });
        expect(card1.id).toBe('card1');
        expect(card1.name).toBe('Card 1');
    });

    it('should self identify', () => {
        const card2 = new Card('card2', { name: 'Card 2' });
        expect(card2.is('card2')).toBe(true);
    });
});
