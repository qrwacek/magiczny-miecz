import { EventEmitter } from "eventemitter3";
import { ICardConfig } from "./card.const";

export class Card<
    ID extends string = string,
    CC extends ICardConfig = ICardConfig,
> extends EventEmitter implements ICardConfig {
    public name: string;
    public description: string;
    public image: string;

    public constructor(public id: ID, cardConfig: CC) {
        super();

        if (!cardConfig) {
            throw new Error(`Config for "${id}" card not found`);
        }

        this.name = cardConfig.name;
        this.description = cardConfig.description || "";
        this.image = cardConfig.image || "";
    }

    public is(id: ID) {
        return this.id === id;
    }

    public isOneOf(types: ID[]) {
        return types.some(this.is);
    }
}
