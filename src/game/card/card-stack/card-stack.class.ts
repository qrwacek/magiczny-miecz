import { Card } from '../card.class';

export class CardStack<T extends Card> {
    protected activeCards: T[] = [];
    protected usedCards: T[];

    public constructor(cards: T[]) {
        this.usedCards = cards;
        this.shuffle();
    }

    public shuffle() {
        this.activeCards = this.usedCards;
        this.activeCards.sort(() => 0.5 - Math.random());
        this.usedCards = [];
    }

    public draw(nrOfCards: number = 1): T[] {
        if (nrOfCards <= this.activeCards.length) {
            return this._getCards(nrOfCards);
        } else {
            const drawnCards = this._getCards(this.activeCards.length);
            const nrOfCardsMissing = nrOfCards - this.activeCards.length;
            this.shuffle();
            return drawnCards.concat(this._getCards(nrOfCardsMissing));
        }
    }

    public putBack(card: T) {
        this.usedCards.push(card);
    }

    protected _getCards(nrOfCards: number) {
        return this.activeCards.splice(0, nrOfCards);
    }
}
