declare module "boardgame.io/core" {
    import { Plugin } from "boardgame.io/plugins";

    /**
     * Default type arguments value
     */
    export type DefaultGameState = object;
    export type DefaultMoves = object;
    export type DefaultGameStatePlayerView = object;
    export type DefaultPlayerID = string;
    export type DefaultPhaseID = string;
    export type DefaultStageID = string;

    export interface ActivePlayers<
        PlayerID extends DefaultPlayerID = DefaultPlayerID,
        StageID extends DefaultStageID = DefaultStageID,
    > {
        player?: StageID;
        others?: StageID;
        all?: StageID;
        value?: Record<PlayerID, StageID>;
        moveLimit?: number;
        revert?: boolean;
        next?: ActivePlayers<PlayerID, StageID>;
    }

    export interface Context<
        PlayerID extends DefaultPlayerID = DefaultPlayerID,
        PhaseID extends DefaultPhaseID = DefaultPhaseID,
        StageID extends DefaultStageID = DefaultStageID,
    > {
        numPlayers: number;
        turn: number;
        currentPlayer: PlayerID;
        playOrder: PlayerID[];
        playOrderPos: number;
        phase: PhaseID | null;
        activePlayers: ActivePlayers<PlayerID, StageID>['value'] | null;
        numMoves: number;
        random: {
            Shuffle: <A extends any[]>(array: A) => A;
            Die: (spotValue: number, diceCount: number) => number[];
            Die: (spotValue: number) => number;
            Number: () => number;
        };
        events: {
            endGame: () => void;
            endPhase: (options?: { next: PhaseID }) => void;
            endTurn: (options?: { next: PlayerID }) => void;
            endStage: () => void;
        };
        playerID: PlayerID;
    }

    export type Hook<
        GameState extends DefaultGameState = DefaultGameState,
        PlayerID extends DefaultPlayerID = DefaultPlayerID,
        PhaseID extends DefaultPhaseID = DefaultPhaseID,
    > = (G: GameState, ctx: Context<PlayerID, PhaseID>) => (GameState | void);

    export interface Hooks<
        GameState extends DefaultGameState = DefaultGameState,
        PlayerID extends DefaultPlayerID = DefaultPlayerID,
        PhaseID extends DefaultPhaseID = DefaultPhaseID,
        NextID extends any = unknown,
    > {
        onBegin?: Hook<GameState, PlayerID, PhaseID>;
        onEnd?: Hook<GameState, PlayerID, PhaseID>;
        endIf?: (
            G: GameState,
            ctx: Context<PlayerID, PhaseID>
        ) => boolean | { next: NextID };
    }

    export type GameMoveHandler<GameState, PlayerID, PhaseID, MoveFn> = (
        G: GameState,
        ctx: Context<PlayerID, PhaseID>,
        ...args: Parameters<MoveFn>
    ) => GameState | void;

    export type GameMoves<
        GameState extends DefaultGameState = DefaultGameState,
        Moves extends DefaultMoves = DefaultMoves,
        PlayerID extends DefaultPlayerID = DefaultPlayerID,
        PhaseID extends DefaultPhaseID = DefaultPhaseID,
    > = {
        [key in keyof Partial<Moves>]: GameMoveHandler<
            GameState,
            PlayerID,
            PhaseId,
            Moves[key],
        >;
    }

    export interface GameTurn<
        GameState extends DefaultGameState = DefaultGameState,
        Moves extends DefaultMoves = DefaultMoves,
        PlayerID extends DefaultPlayerID = DefaultPlayerID,
        PhaseID extends DefaultPhaseID = DefaultPhaseID,
        StageID extends DefaultStageID = DefaultStageID,
    > extends Hooks<GameState, PlayerID, PhaseID, PlayerID> {
        order?: any; // TODO: add types
        onMove?: Hook<GameState, PlayerID, PhaseID>;
        moveLimit?: number;
        activePlayers?: ActivePlayers<PlayerID, StageID>;
        stages?: Partial<Record<StageID, {
            moves?: GameMoves<GameState, Moves, PlayerID, PhaseID>;
            next?: StageID;
        }>>;
    }

    export interface GameConfig<
        GameState extends DefaultGameState = DefaultGameState,
        GameStatePlayerView extends DefaultGameStatePlayerView = DefaultGameStatePlayerView,
        Moves extends DefaultMoves = DefaultMoves,
        PlayerID extends DefaultPlayerID = DefaultPlayerID,
        PhaseID extends DefaultPhaseID = DefaultPhaseID,
        StageID extends DefaultStageID = DefaultStageID,
    > {
        name: string;

        setup: (
            ctx: Context<PlayerID, PhaseID>,
            setupData: object
        ) => GameState;

        moves: GameMoves<GameState, Moves, PlayerID, PhaseID>;

        playerView?: (
            G: GameState,
            ctx: Context<PlayerID, PhaseID>,
            playerID: PlayerID
        ) => GameStatePlayerView;

        seed?: string;

        turn?: GameTurn<GameState, Moves, PlayerID, PhaseID, StageID>;

        phases?: {
            [key in PhaseID]: Hooks<GameState, PlayerID, PhaseID, PhaseID> & {
                moves?: GameMoves<GameState, Moves, PlayerID, PhaseID>;
                turn?: GameTurn<GameState, Moves, PlayerID, PhaseID, StageID>;
                start?: boolean,
                next?: PhaseID,
            }
        };

        endIf?: (G: GameState, ctx: Context<PlayerID, PhaseID>) => boolean;

        plugins?: Array<Plugin<GameState, Moves, PlayerID, PhaseID>>;
        playerSetup?: (playerId: PlayerID) => object;
    }

    export function Game<
        GameState extends DefaultGameState = DefaultGameState,
        GameStatePlayerView extends DefaultGameStatePlayerView = DefaultGameStatePlayerView,
        Moves extends DefaultMoves = DefaultMoves,
        PlayerID extends DefaultPlayerID = DefaultPlayerID,
        PhaseID extends DefaultPhaseID = DefaultPhaseID,
        StageID extends DefaultStageID = DefaultStageID,
    >(
        game: GameConfig<
            GameState,
            GameStatePlayerView,
            Moves,
            PlayerID,
            PhaseID,
            StageID,
        >
    ): object;

    export const TurnOrder: {
        CUSTOM_FROM: <GameState = DefaultGameState>(
            gameStateKey: keyof GameState
        ) => object;
    };

    export const PlayerView: {
        STRIP_SECRETS: Required<GameConfig>["playerView"];
    };
}

declare module "boardgame.io/react" {
    import { ComponentType } from "react";
    import {
        Context,
        DefaultGameStatePlayerView,
        DefaultMoves,
        DefaultPlayerID,
        DefaultPhaseID,
        TurnOrder
    } from "boardgame.io/core";

    export interface LobbyProps {
        gameServer: string;
        lobbyServer: string;
        gameComponents: Array<{
            game: object;
            board?: ComponentType<
                BoardProps<GameStatePlayerView, Moves, PlayerID, PhaseID>
            >;
        }>
    }

    export const Lobby: ComponentType<LobbyProps>;

    export interface BoardProps<
        GameStatePlayerView extends DefaultGameStatePlayerView = DefaultGameStatePlayerView,
        Moves extends DefaultMoves = DefaultMoves,
        PlayerID extends DefaultPlayerID = DefaultPlayerID,
        PhaseID extends DefaultPhaseID = DefaultPhaseID,
        StageID extends DefaultStageID = DefaultStageID,
    > {
        G: GameStatePlayerView;
        ctx: Context<PlayerID, PhaseID, StageID>;
        moves: Moves;
        gameID: string;
        playerID: PlayerID;
        isActive: boolean;
        isMultiplayer: boolean;
        isConnected: boolean;
    }

    export interface Client<
        GameStatePlayerView extends DefaultGameStatePlayerView = DefaultGameStatePlayerView,
        Moves extends DefaultMoves = DefaultMoves,
        PlayerID extends DefaultPlayerID = DefaultPlayerID,
        PhaseID extends DefaultPhaseID = DefaultPhaseID,
        StageID extends DefaultStageID = DefaultStageID,
    > {
        game: object;
        numPlayers?: number;
        board?: ComponentType<
            BoardProps<GameStatePlayerView, Moves, PlayerID, PhaseID>
        >;
        multiplayer?: boolean | { server: string } | { local: boolean };
        debug?: boolean;
    }

    export interface ClientProps<PlayerID = DefaultPlayerID> {
        gameID?: string;
        playerID?: PlayerID;
        debug?: boolean;
    }

    export function Client<
        GameStatePlayerView extends DefaultGameStatePlayerView = DefaultGameStatePlayerView,
        Moves extends DefaultMoves = DefaultMoves,
        PlayerID extends DefaultPlayerID = DefaultPlayerID,
        PhaseID extends DefaultPhaseID = DefaultPhaseID,
        StageID extends DefaultStageID = DefaultStageID,
    >(
        client: Client<GameStatePlayerView, Moves, PlayerID, PhaseID, StageID>
    ): ComponentType<ClientProps<PlayerID>>;
}

declare module "boardgame.io/plugins" {
    import {
        Context,
        DefaultGameState,
        DefaultMoves,
        DefaultPlayerID,
        DefaultPhaseID,
    } from "boardgame.io/core";

    export interface Plugin<
        GameState extends DefaultGameState = DefaultGameState,
        Moves extends DefaultMoves = DefaultMoves,
        PlayerID extends DefaultPlayerID = DefaultPlayerID,
        PhaseID extends DefaultPhaseID = DefaultPhaseID,
    > {
        fnWrap?: (moveFn: (
            G: GameState,
            ctx: Context<PlayerID, PhaseID>,
            ...args: any[],
        ) => GameState | void) => ((
            G: GameState,
            ctx: Context<PlayerID, PhaseID>,
            ...args: any[]
        ) => void);

        G: {
            setup: (
                G: GameState,
                ctx: Context<PlayerID, PhaseID>,
            ) => G,
        },
    }

    export const PluginPlayer: Plugin;
}

declare module "boardgame.io/server" {
    export interface ServerConfig {
        games: object[];
        db?: object;
        transport?: object;
    }

    export function Server(config: ServerConfig): {
        run: (port: number, callback: () => void) => void,
    }
}
