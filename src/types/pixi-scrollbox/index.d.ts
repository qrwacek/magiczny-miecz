declare module 'pixi-scrollbox' {
    import * as PIXI from 'pixi.js';
    import { Viewport, UnderflowType } from 'pixi-viewport';

    type OverflowType = 'none' | 'scroll' | 'hidden' | 'auto';

    interface IScrollboxOptions {
        dragScroll?: boolean;
        overflowX?: OverflowType;
        overflowY?: OverflowType;
        overflow?: OverflowType;
        boxWidth?: number;
        boxHeight?: number;
        scrollbarSize?: number;
        scrollbarOffsetHorizontal?: number;
        scrollbarOffsetVertical?: number;
        stopPropagation?: boolean;
        scrollbarBackground?: number;
        scrollbarBackgroundAlpha?: number;
        scrollbarForeground?: number;
        scrollbarForegroundAlpha?: number;
        underflow?: UnderflowType;
        noTicker?: boolean;
        ticker?: PIXI.Ticker;
        fade?: boolean;
        fadeScrollbarTime?: number;
        fadeScrollbarWait?: number;
        fadeScrollboxEase?: string | ((...args: any[]) => any);
        passiveWheel?: boolean;
        clampWheel?: boolean;
    }

    interface IResizeOptions {
        boxWidth?: number;
        boxHeight?: number;
        scrollWidth?: number;
        scrollHeight?: number;
    }

    export class Scrollbox extends PIXI.Container {
        constructor(options?: IScrollboxOptions);

        boxHeight: number;
        boxWidth: number;
        content: Viewport;
        contentHeight: number;
        contentWidth: number;
        dirty: boolean;
        disable: boolean;
        dragScroll: boolean;
        isScrollbarHorizontal: boolean;
        isScrollbarVertical: boolean;
        overflow: OverflowType;
        overflowX: OverflowType;
        overflowY: OverflowType;
        scrollbar: PIXI.Graphics;
        scrollbarOffsetHorizontal: number;
        scrollbarOffsetVertical: number;
        scrollbarSize: number;
        scrollHeight: number;
        scrollLeft: number;
        scrollTop: number;
        scrollWidth: number;
        stopPropagation: boolean;

        activateFade(): void;
        ensureVisible(x: number, y: number, width: number, height: number): void;
        resize(options: IResizeOptions): void;
        update(): void;
        updateLoop(elapsed: number): void;
    }
}
